Szakdolgozat

Seasonal fruit and vegatables Android App

* Firebase Real Time Database online adatok tárolására, Android Room a lokális adatok tárolására
* Szinkronizálás történik a két adatbázis között
* Megjelennek a Magyarországon szezonálisan kapható zöldségek és gyümölcsök adatai
* Lehetőség van felíratkozni értesítésekre, amikor kaphatók lesznek a növények az üzletekben
* Tartalmaz egy naptár funkciót, amiben el lehet menteni a gyümölcsök és zöldségek szezonális időszakát
