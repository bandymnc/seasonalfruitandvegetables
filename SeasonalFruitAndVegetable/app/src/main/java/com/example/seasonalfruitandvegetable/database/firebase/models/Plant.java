package com.example.seasonalfruitandvegetable.database.firebase.models;

import java.util.List;

public class Plant {
    public Long id;
    public String name;
    public boolean deleted;
    public String maturePicture;
    public String immaturePicture;
    public String endDate;
    public String startDate;
    private String description;
    private String other;
    private Long plantTypeId;
    private List<Vitamin> vitaminList;
    private String color;

    public Plant(Long id, String name, boolean deleted, String maturePicture, String immaturePicture, String endDate, String startDate, String description, String other, Long plantTypeId, List<Vitamin> vitaminList, String color) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
        this.maturePicture = maturePicture;
        this.immaturePicture = immaturePicture;
        this.endDate = endDate;
        this.startDate = startDate;
        this.description = description;
        this.other = other;
        this.plantTypeId = plantTypeId;
        this.vitaminList = vitaminList;
        this.color = color;
    }

    public Plant() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getMaturePicture() {
        return maturePicture;
    }

    public void setMaturePicture(String maturePicture) {
        this.maturePicture = maturePicture;
    }

    public String getImmaturePicture() {
        return immaturePicture;
    }

    public void setImmaturePicture(String immaturePicture) {
        this.immaturePicture = immaturePicture;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Long getPlantTypeId() {
        return plantTypeId;
    }

    public void setPlantTypeId(Long plantTypeId) {
        this.plantTypeId = plantTypeId;
    }

    public List<Vitamin> getVitaminList() {
        return vitaminList;
    }

    public void setVitaminList(List<Vitamin> vitaminList) {
        this.vitaminList = vitaminList;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
