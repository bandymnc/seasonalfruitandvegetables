package com.example.seasonalfruitandvegetable.ui.calendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.VitaminRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalendarFragment extends Fragment {

    private CalendarViewModel calendarViewModel;
    private ImageButton nextButton;
    private ImageButton previousButton;
    private TextView currentDate;
    private GridView gridView;
    private CalendarAdapter calendarAdapter;
    private static final int MAX_CALENDAR_DAYS = 42;
    private Calendar calendar = Calendar.getInstance(Locale.ENGLISH);

    private List<Date> dateList = new ArrayList<>();
    private List<PlantEntity> plantEntityList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        calendarViewModel = ViewModelProviders.of(this).get(CalendarViewModel.class);

        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        calendarViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors),new VitaminRepository(appDatabase,appExecutors));
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getLayoutItems();
        clickListeners();
        setUpCalendar();
        gridViewClickListener();
    }

    private void getLayoutItems() {
        nextButton = getView().findViewById(R.id.nextButton);
        previousButton = getView().findViewById(R.id.previousButton);
        currentDate = getView().findViewById(R.id.currentDate);
        gridView = getView().findViewById(R.id.gridView);
    }

    private void clickListeners() {
        previousButton.setOnClickListener((View v) -> {
            calendar.add(Calendar.MONTH, -1);
            setUpCalendar();
        });

        nextButton.setOnClickListener((View v) -> {
            calendar.add(Calendar.MONTH, 1);
            setUpCalendar();
        });
    }

    private void gridViewClickListener() {
        gridView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {
            Bundle bundle = new Bundle();
            String date = CommonUtils.getDatabaseDateFormat(dateList.get(position));
            bundle.putString("date", date);

            if (getActivity() != null && isPlantsThatDay(date)) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                CalendarDateDialogFragment calendarDateDialogFragment = new CalendarDateDialogFragment();
                calendarDateDialogFragment.setArguments(bundle);
                calendarDateDialogFragment.show(fm, "calendar_dialog");
            }else{
                CustomSnackBar customSnackBar = new CustomSnackBar(getContext(),getView());
                customSnackBar.showSnackBar(getString(R.string.no_plants_in_this_day),true);
            }
        });
    }

    private boolean isPlantsThatDay(String date){

        for(PlantEntity plantEntity: plantEntityList){
            if((plantEntity.getStartDate().compareTo(date)) < 1 && (plantEntity.getEndDate().compareTo(date)) > -1){
                return true;
            }
        }
        return false;
    }

    private void setUpCalendar() {
        Calendar monthCalendar = (Calendar) calendar.clone();
        currentDate.setText(CommonUtils.getHungarianYearAndMonthFormat(calendar.getTime()));
        dateList.clear();
        plantEntityList.clear();
        monthCalendar.set(Calendar.DAY_OF_MONTH, 0);
        int firstDayOfMonth = monthCalendar.get(Calendar.DAY_OF_WEEK) - 2;
        monthCalendar.add(Calendar.DAY_OF_MONTH, -firstDayOfMonth);

        while (dateList.size() < MAX_CALENDAR_DAYS) {
            dateList.add(monthCalendar.getTime());
            monthCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        calendarViewModel.getAllActivePlant().observe(this, plants -> {
            plantEntityList = plants;

            calendarAdapter = new CalendarAdapter(getContext(), dateList, calendar, plantEntityList);
            gridView.setAdapter(calendarAdapter);
        });
    }
}
