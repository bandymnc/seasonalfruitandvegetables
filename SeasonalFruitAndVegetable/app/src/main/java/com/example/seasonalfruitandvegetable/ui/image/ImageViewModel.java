package com.example.seasonalfruitandvegetable.ui.image;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;

public class ImageViewModel extends ViewModel {
    private PlantRepository plantRepository;

    public ImageViewModel() {
    }

    public void setDependencies(PlantRepository plantRepository){
        this.plantRepository = plantRepository;
    }

    public LiveData<PlantEntity> getPlantById(Long id){
        return plantRepository.getPlantById(id);
    }
}
