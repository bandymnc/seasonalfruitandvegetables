package com.example.seasonalfruitandvegetable.ui.notification_dialog;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.NotificationRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.example.seasonalfruitandvegetable.helper.alarm.AlarmHelper;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class NotificationDialogFragment extends DialogFragment {

    private NotificationDialogViewModel notificationDialogViewModel;
    private EditText noticeDialogDate;
    private DatePickerDialog mDatePickerDialog;
    private TextView noticeDialogTitle;
    private PlantEntity plantEntity;
    private NotificationEntity notificationEntity;
    private TextView startDate;
    private TextView endDate;
    private Button saveButton;
    private Button cancelButton;
    private ImageButton deleteNotificationButton;
    private Long requestCode;
    private AlarmHelper alarmHelper;
    private Calendar chosenDateCalendar;
    private CustomSnackBar customSnackBar;
    private boolean isAdapterOpened = false;

    public NotificationDialogFragment() {
        // Required empty public constructor
    }

    public static NotificationDialogFragment newInstance(String title) {
        NotificationDialogFragment frag = new NotificationDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        notificationDialogViewModel = ViewModelProviders.of(this).get(NotificationDialogViewModel.class);
        notificationDialogViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors),new NotificationRepository(appDatabase,appExecutors));

        return inflater.inflate(R.layout.fragment_notification_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customSnackBar = new CustomSnackBar(getContext(), getView());
        getLayoutItems();
        Long plantId = getArguments().getLong("plantId", 0L);
        isAdapterOpened = getArguments().getBoolean("isAdapter");
        getPlantById(plantId);
        cancelButtonChangeListener();
        deleteButtonClickListener();
        saveButtonClickListener();
        alarmHelper = new AlarmHelper(getContext());
    }

    private void getLayoutItems() {
        noticeDialogDate = getView().findViewById(R.id.noticeDateEditText);
        noticeDialogTitle = getView().findViewById(R.id.noticeDialogTitle);
        saveButton = getView().findViewById(R.id.fragmentNoticeDialogSaveButton);
        cancelButton = getView().findViewById(R.id.fragmentNoticeDialogCancelButton);
        deleteNotificationButton = getView().findViewById(R.id.fragmentNoticeDialogDeleteButton);
        startDate = getView().findViewById(R.id.fragmentNoticeDialogSeasonStartDate);
        endDate = getView().findViewById(R.id.fragmentNoticeDialogSeasonEndDate);
    }

    private void getPlantById(Long id) {
        notificationDialogViewModel.getPlantById(id).observe(this, plant -> {
            if (plant != null) {
                plantEntity = plant;
                setTexts();
                getNotification();
                requestCode = plant.getId();
            } else {
                dismiss();
            }
        });
    }

    private void cancelButtonChangeListener() {
        cancelButton.setOnClickListener((View v) -> {
            dismiss();
        });
    }

    private void setTexts() {
        noticeDialogTitle.setText(plantEntity.getName());
        startDate.setText(getString(R.string.season_start) + " : " + CommonUtils.getHungarianDateFormat(plantEntity.getStartDate()));
        endDate.setText(getString(R.string.season_end) + " : " + CommonUtils.getHungarianDateFormat(plantEntity.getEndDate()));
    }

    private void getNotification() {
        notificationDialogViewModel.getNotificationByPlantId(plantEntity.getId()).observe(this, notification -> {
            deleteNotificationButton.setEnabled(false);

            if (notification != null) {
                noticeDialogDate.setText(CommonUtils.getHungarianDateFormat(notification.getNotificationDate()));
                notificationEntity = notification;
                deleteNotificationButton.setEnabled(true);
            }

            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(CommonUtils.getDateFromDbDateFormat(plantEntity.getStartDate()));
            startCalendar.add(Calendar.DAY_OF_WEEK, -1);

            setDateTimeField(startCalendar);
            editTextChangeListener();
        });
    }

    private void editTextChangeListener() {
        noticeDialogDate.setOnClickListener((View v) -> {

            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(CommonUtils.getDateFromDbDateFormat(plantEntity.getStartDate()));
            startCalendar.add(Calendar.DAY_OF_WEEK, -1);

            if (startCalendar.compareTo(Calendar.getInstance()) > 0) {
                mDatePickerDialog.show();
            } else {
                customSnackBar.showSnackBar(getString(R.string.cant_modify), true);
            }
        });
    }

    private void deleteButtonClickListener() {
        deleteNotificationButton.setOnClickListener((View v) -> {
            confirmationDialog(getString(R.string.delete_confirmation_title), getString(R.string.delete_notification_confirmation), true);
        });
    }

    private void saveButtonClickListener() {
        saveButton.setOnClickListener((View v) -> {
            confirmationDialog(getString(R.string.save_confirmation_title), getString(R.string.save_confirmation), false);
        });
    }

    private void setDateTimeField(Calendar startCalendar) {

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(Calendar.getInstance().getTime());

        mDatePickerDialog = new DatePickerDialog(getContext(), (DatePicker view, int year, int monthOfYear, int dayOfMonth) -> {

            chosenDateCalendar = Calendar.getInstance();
            chosenDateCalendar.set(year, monthOfYear, dayOfMonth);
            openTimePicker();
        }, startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMaxDate(startCalendar.getTimeInMillis());
        currentCalendar.add(Calendar.DAY_OF_WEEK, 1);
        mDatePickerDialog.getDatePicker().setMinDate(currentCalendar.getTimeInMillis());
    }


    private void confirmationDialog(String title, String msg, boolean isDelete) {

        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle(title);
        alert.setMessage(msg);

        alert.setPositiveButton(android.R.string.yes, (DialogInterface dialog, int which) -> {
            handleConfirmDialogResult(isDelete);
        });

        alert.setNegativeButton(android.R.string.no, (DialogInterface dialog, int which) -> {
            dialog.cancel();
        });
        alert.show();
    }

    private void handleConfirmDialogResult(boolean isDelete) {

        if (isDelete) {
            deleteNotificationButton.setEnabled(false);
            saveButton.setEnabled(false);
            notificationDialogViewModel.delete(notificationEntity);
            noticeDialogDate.getText().clear();
            notificationEntity = null;
            alarmHelper.cancelAlarm(requestCode);

            if (isAdapterOpened) {
                dismiss();
            }

        } else {
            String newDate = CommonUtils.getDateTimeFromDbDate(chosenDateCalendar.getTime());

            Date startDate = CommonUtils.getDateFromDbDateFormat(plantEntity.getStartDate());
            Date notifyDate = chosenDateCalendar.getTime();

            FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
            Bundle bundle = new Bundle();
            bundle.putString("days_before", Long.toString(TimeUnit.MILLISECONDS.toDays(startDate.getTime() - notifyDate.getTime())));
            bundle.putString("plantName", plantEntity.getName());
            firebaseAnalytics.logEvent("notification", bundle);

            if (notificationEntity == null) {
                notificationDialogViewModel.insert(new NotificationEntity(newDate, plantEntity.getId()));
                alarmHelper.setAlarm(requestCode, plantEntity, chosenDateCalendar);
                saveButton.setEnabled(false);
            } else {
                notificationEntity.setNotificationDate(newDate);
                notificationDialogViewModel.update(notificationEntity);
                alarmHelper.modifyAlarm(requestCode, plantEntity, chosenDateCalendar);
                saveButton.setEnabled(false);
            }
        }
    }

    private void openTimePicker() {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), (TimePicker timePicker, int selectedHour, int selectedMinute) -> {
            chosenDateCalendar.set(Calendar.HOUR_OF_DAY, selectedHour);
            chosenDateCalendar.set(Calendar.MINUTE, selectedMinute);
            noticeDialogDate.setText(CommonUtils.getHungarianDateTimeFormat(chosenDateCalendar.getTime()));
            saveButton.setEnabled(true);
            deleteNotificationButton.setEnabled(true);
        }, hour, minute, true);
        mTimePicker.show();
    }
}
