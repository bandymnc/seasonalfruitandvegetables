package com.example.seasonalfruitandvegetable.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView homeRecyclerView;
    private TextView titleTextView;
    private Date todayDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors));

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeRecyclerView = getView().findViewById(R.id.homeRecyclerView);
        titleTextView = getView().findViewById(R.id.fragmentHomeTitle);
        todayDate = Calendar.getInstance().getTime();
        titleTextView.setText(getString(R.string.home_fragment_title_first_part) + "\n" + CommonUtils.getHungarianDateFormat(todayDate) + "\n" + getString(R.string.home_fragment_title_last_part));

        homeViewModel.getAllAvailablePlants(CommonUtils.getDatabaseDateFormat(todayDate)).observe(this, plants -> {

            if (plants != null) {
                HomeAdapter homeAdapter = new HomeAdapter(plants, getContext());
                homeRecyclerView.setAdapter(homeAdapter);
                homeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            }
        });
    }
}

