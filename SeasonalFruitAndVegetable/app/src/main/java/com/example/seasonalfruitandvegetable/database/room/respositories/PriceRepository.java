package com.example.seasonalfruitandvegetable.database.room.respositories;

import androidx.lifecycle.LiveData;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.PriceDao;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PricesWithPlantAndAmountUnit;
import com.example.seasonalfruitandvegetable.database.room.entities.PriceEntity;

import java.util.List;

public class PriceRepository {
    private AppExecutors appExecutors;
    private PriceDao priceDao;

    public PriceRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        priceDao = appDatabase.priceDao();
    }

    public void insert(PriceEntity priceEntity) {
        appExecutors.databaseIO().execute(() -> {
            priceDao.insert(priceEntity);
        });
    }

    public void update(PriceEntity priceEntity) {
        appExecutors.databaseIO().execute(() -> {
            priceDao.update(priceEntity);
        });
    }

    public LiveData<List<PricesWithPlantAndAmountUnit>> getAllPricesWithPlantAndAmountUnit(){
        return priceDao.getAllPricesWithPlantAndAmountUnit();
    }

    public LiveData<List<PricesWithPlantAndAmountUnit>> getAllPricesWithPlantAndAmountUnitByPlantName(String plantName){
        return priceDao.getAllPricesWithPlantAndAmountUnitByPlantName(plantName);
    }
}
