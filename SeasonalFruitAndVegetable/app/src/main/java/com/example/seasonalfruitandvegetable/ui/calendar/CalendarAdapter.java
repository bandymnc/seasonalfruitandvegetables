package com.example.seasonalfruitandvegetable.ui.calendar;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.data_objects.CalendarPlants;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarAdapter extends ArrayAdapter {

    List<Date> dates;
    Calendar currentDate;
    List<PlantEntity> plantEntities;
    LayoutInflater inflater;
    Context context;

    public CalendarAdapter(@NonNull Context context, List<Date> dates, Calendar currentDate, List<PlantEntity> plantEntities) {
        super(context, R.layout.single_date_cell);
        this.dates = dates;
        this.currentDate = currentDate;
        this.plantEntities = plantEntities;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        Date monthDate = dates.get(position);
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(monthDate);
        int displayDate = dateCalendar.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCalendar.get(Calendar.MONTH) + 1;
        int displayYear = dateCalendar.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        TypedValue inactiveDayTextColor = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.calendarInActiveMonthDayColor, inactiveDayTextColor, true);
        TypedValue firstLineColor = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.firstLineColor, firstLineColor, true);
        TypedValue secondLineColor = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.secondLineColor, secondLineColor, true);

        if (view == null) {
            view = inflater.inflate(R.layout.single_date_cell, parent, false);
        }

        TextView numberOfDate = view.findViewById(R.id.singleDateCellCalendarDay);
        TextView firstEvent = view.findViewById(R.id.singleDateCellFirstEvent);
        TextView secondEvent = view.findViewById(R.id.singleDateCellSecondEvent);
        TextView thirdEvent = view.findViewById(R.id.singleDateCellThirdEvent);
        TextView otherEvent = view.findViewById(R.id.singleDateCellOtherEvent);
        numberOfDate.setText(String.valueOf(displayDate));

        if (displayMonth != currentMonth || displayYear != currentYear) {
            numberOfDate.setTextColor(inactiveDayTextColor.data);
        }

        String currentDate = CommonUtils.getDatabaseDateFormat(monthDate);
        String lastDay = CommonUtils.getDatabaseDateFormat(monthDate);

        if (position != 0) {
            lastDay = CommonUtils.getDatabaseDateFormat(dates.get(position - 1));
        }

        CalendarDataHelper calendarDataHelper = new CalendarDataHelper();

        CalendarPlants currentPlants = calendarDataHelper.getPlants(plantEntities, currentDate,lastDay);


        if(calendarDataHelper.writeText(currentPlants.getToDayPlantTmb()[0],currentPlants.getLastDayPlantTmb()[0],currentDate,dateCalendar)){
            firstEvent.setText(currentPlants.getToDayPlantTmb()[0].getName());
        }
        if(calendarDataHelper.setBackgroundColor(currentPlants.getToDayPlantTmb()[0],currentPlants.getLastDayPlantTmb()[0],currentDate,dateCalendar)){
            firstEvent.setBackgroundColor(Color.parseColor(currentPlants.getToDayPlantTmb()[0].getColor()));
        }

        if(calendarDataHelper.writeText(currentPlants.getToDayPlantTmb()[1],currentPlants.getLastDayPlantTmb()[1],currentDate,dateCalendar)){
            secondEvent.setText(currentPlants.getToDayPlantTmb()[1].getName());
        }
        if(calendarDataHelper.setBackgroundColor(currentPlants.getToDayPlantTmb()[1],currentPlants.getLastDayPlantTmb()[1],currentDate,dateCalendar)){
            secondEvent.setBackgroundColor(Color.parseColor(currentPlants.getToDayPlantTmb()[1].getColor()));
        }

        if(calendarDataHelper.writeText(currentPlants.getToDayPlantTmb()[2],currentPlants.getLastDayPlantTmb()[2],currentDate,dateCalendar)){
            thirdEvent.setText(currentPlants.getToDayPlantTmb()[2].getName());
        }
        if(calendarDataHelper.setBackgroundColor(currentPlants.getToDayPlantTmb()[2],currentPlants.getLastDayPlantTmb()[2],currentDate,dateCalendar)){
            thirdEvent.setBackgroundColor(Color.parseColor(currentPlants.getToDayPlantTmb()[2].getColor()));
        }

        if (currentPlants.getOthers() != 0) {
            otherEvent.setText("+ " + currentPlants.getOthers());
            otherEvent.setTextColor(Color.parseColor("#d50000"));
        }

        return view;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public int getPosition(@Nullable Object item) {
        return dates.indexOf(item);
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return dates.get(position);
    }
}