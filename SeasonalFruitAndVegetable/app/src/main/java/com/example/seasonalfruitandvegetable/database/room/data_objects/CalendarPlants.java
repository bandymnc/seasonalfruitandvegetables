package com.example.seasonalfruitandvegetable.database.room.data_objects;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;

import java.util.List;

public class CalendarPlants {
    private PlantEntity[] toDayPlantTmb;
    private PlantEntity[] lastDayPlantTmb;
    private int others;

    public CalendarPlants(PlantEntity[] toDayPlantTmb, PlantEntity[] lastDayPlantTmb, int others) {
        this.toDayPlantTmb = toDayPlantTmb;
        this.lastDayPlantTmb = lastDayPlantTmb;
        this.others = others;
    }

    public CalendarPlants() {
    }

    public PlantEntity[] getToDayPlantTmb() {
        return toDayPlantTmb;
    }

    public void setToDayPlantTmb(PlantEntity[] toDayPlantTmb) {
        this.toDayPlantTmb = toDayPlantTmb;
    }

    public PlantEntity[] getLastDayPlantTmb() {
        return lastDayPlantTmb;
    }

    public void setLastDayPlantTmb(PlantEntity[] lastDayPlantTmb) {
        this.lastDayPlantTmb = lastDayPlantTmb;
    }

    public int getOthers() {
        return others;
    }

    public void setOthers(int others) {
        this.others = others;
    }
}
