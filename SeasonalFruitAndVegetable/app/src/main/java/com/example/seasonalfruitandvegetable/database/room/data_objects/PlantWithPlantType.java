package com.example.seasonalfruitandvegetable.database.room.data_objects;

public class PlantWithPlantType {

    private Long plantId;
    private String typeName;
    private String plantTypeName;
    private String name;
    private boolean active;
    private String description;
    private String other;
    private String immaturePicture;
    private String  maturePicture;
    private String startDate;
    private String endDate;
    private String color;

    public PlantWithPlantType(Long plantId, String typeName, String plantTypeName, String name, boolean active, String description, String other, String immaturePicture, String maturePicture, String startDate, String endDate, String color) {
        this.plantId = plantId;
        this.typeName = typeName;
        this.plantTypeName = plantTypeName;
        this.name = name;
        this.active = active;
        this.description = description;
        this.other = other;
        this.immaturePicture = immaturePicture;
        this.maturePicture = maturePicture;
        this.startDate = startDate;
        this.endDate = endDate;
        this.color = color;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getPlantTypeName() {
        return plantTypeName;
    }

    public void setPlantTypeName(String plantTypeName) {
        this.plantTypeName = plantTypeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getImmaturePicture() {
        return immaturePicture;
    }

    public void setImmaturePicture(String immaturePicture) {
        this.immaturePicture = immaturePicture;
    }

    public String getMaturePicture() {
        return maturePicture;
    }

    public void setMaturePicture(String maturePicture) {
        this.maturePicture = maturePicture;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
