package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;

import java.util.List;

@Dao
public interface DatabaseLastModifiedDao {

    @Insert
    void insert(DatabaseLastModifiedEntity databaseLastModifiedEntity);

    @Insert
    void insert(List<DatabaseLastModifiedEntity> databaseLastModifiedEntityList);

    @Update
    void update(DatabaseLastModifiedEntity databaseLastModifiedEntity);

    @Delete
    void delete(DatabaseLastModifiedEntity databaseLastModifiedEntity);

    @Query("SELECT * FROM databases_last_modified WHERE databases_last_modified.database_type = :databaseType")
    LiveData<DatabaseLastModifiedEntity> getLastModifiedDatabase(String databaseType);

    @Query("SELECT * FROM databases_last_modified")
    LiveData<List<DatabaseLastModifiedEntity>> getLastModifiedDatabases();

    @Query("SELECT * FROM databases_last_modified WHERE databases_last_modified.database_type = :databaseType")
    DatabaseLastModifiedEntity getLastModifiedDatabaseSyncron(String databaseType);

}
