package com.example.seasonalfruitandvegetable.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.VitaminRepository;

import java.util.List;

public class GalleryViewModel extends ViewModel {

    private PlantRepository plantRepository;
    private VitaminRepository vitaminRepository;

    public GalleryViewModel() {
    }

    public void setDependencies(PlantRepository plantRepository,VitaminRepository vitaminRepository){
        this.plantRepository = plantRepository;
        this.vitaminRepository = vitaminRepository;
    }

    public LiveData<List<PlantEntity>> getPlantsByType(String plantType){
        return plantRepository.getPlantsByType(plantType);
    }

    public LiveData<PlantEntity> getPlantById(Long id){
        return plantRepository.getPlantById(id);
    }

    public LiveData<List<VitaminEntity>> getVitaminsByPlantId(Long id){
        return vitaminRepository.getVitaminsByPlantId(id);
    }

    public void updatePlant(PlantEntity plantEntity){
        plantRepository.update(plantEntity);
    }

    public LiveData<List<VitaminEntity>> getAllVitamin(){
        return vitaminRepository.getAllVitamin();
    }

    public LiveData<List<PlantEntity>> getPlantsByVitamins(String[] vitamins, String plantType, int size){
        return plantRepository.getPlantsByVitamins(vitamins,plantType,size);
    }

    public LiveData<List<PlantEntity>> getAllPlant(){
        return plantRepository.getAllPlant();
    }
}
