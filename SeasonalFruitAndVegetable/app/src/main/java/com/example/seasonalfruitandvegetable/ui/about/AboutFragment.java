package com.example.seasonalfruitandvegetable.ui.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.firebase.FirebaseService;
import com.example.seasonalfruitandvegetable.database.firebase.models.Messeage;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.Calendar;

public class AboutFragment extends Fragment {
    private Button sendButton;
    private EditText messageEditText;
    private EditText titleEditText;
    private FirebaseService firebaseService;
    private GoogleSignInAccount googleSignInAccount;
    private CustomSnackBar customSnackBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CommonUtils.hideKeyboard(getView(), getActivity());
        firebaseService = new FirebaseService(getContext(), getActivity());
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(getContext());
        customSnackBar = new CustomSnackBar(getContext(),getView());

        getLayoutItems();
        buttonClickListener();
    }

    private void buttonClickListener() {

        sendButton.setOnClickListener((View v) -> {

            if (CommonUtils.isOnline(getContext())) {
                if (messageEditText.getText().length() != 0 && titleEditText.getText().length() != 0) {
                    firebaseService.writeData(new Messeage(
                            titleEditText.getText().toString(),
                            messageEditText.getText().toString(),
                            googleSignInAccount.getEmail(),
                            CommonUtils.getDateTimeFromDbDate(Calendar.getInstance().getTime())));

                    messageEditText.getText().clear();
                    titleEditText.getText().clear();
                    customSnackBar.showSnackBar(getString(R.string.message_send_succesfull), false);
                } else {
                    customSnackBar.showSnackBar(getString(R.string.fill_field), true);
                }
            } else {
                customSnackBar.showSnackBar(getString(R.string.turn_on_internet), true);
            }
        });
    }

    private void getLayoutItems() {
        sendButton = getView().findViewById(R.id.aboutFragmentButton);
        messageEditText = getView().findViewById(R.id.aboutFragmentBodyEditText);
        titleEditText = getView().findViewById(R.id.aboutFragmentTitleEditText);
    }
}

