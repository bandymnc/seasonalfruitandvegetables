package com.example.seasonalfruitandvegetable.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;

import java.util.List;

public class HomeViewModel extends ViewModel {

    private PlantRepository plantRepository;

    public HomeViewModel() {
    }

    public void setDependencies(PlantRepository plantRepository){
        this.plantRepository = plantRepository;
    }

    public LiveData<List<PlantEntity>> getAllAvailablePlants(String todayDate){
        return plantRepository.getAllAvailablePlants(todayDate);
    }

    public void insert(PlantEntity plantEntity){
        plantRepository.insert(plantEntity);
    }

}
