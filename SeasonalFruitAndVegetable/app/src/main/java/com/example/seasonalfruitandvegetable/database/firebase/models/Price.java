package com.example.seasonalfruitandvegetable.database.firebase.models;

public class Price {

    private Long id;
    private String amountType;
    private String description;
    private int minPrice;
    private int maxPrice;
    private Long plantId;

    public Price(Long id, String amountType, String description, int minPrice, int maxPrice, Long plantId) {
        this.id = id;
        this.amountType = amountType;
        this.description = description;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.plantId = plantId;
    }

    public Price() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }
}
