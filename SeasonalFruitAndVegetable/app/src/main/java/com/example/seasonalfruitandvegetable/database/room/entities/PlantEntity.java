package com.example.seasonalfruitandvegetable.database.room.entities;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.NO_ACTION;

@Entity(tableName = "plants" ,
        foreignKeys ={
                @ForeignKey(
                        entity = PlantTypeEntity.class,
                        parentColumns = "id",
                        childColumns = "plant_type_id")})
public class PlantEntity {

    @PrimaryKey
    private Long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "active")
    private boolean active;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "other")
    private String other;

    @ColumnInfo(name = "color")
    private String color;

    @ColumnInfo(name = "immature_picture")
    private String immaturePicture;

    @ColumnInfo(name = "mature_picture")
    private String maturePicture;

    @ColumnInfo(name = "start_date")
    private String startDate;

    @ColumnInfo(name = "end_date")
    private String endDate;

    @ColumnInfo(name = "plant_type_id")
    private Long plantTypeId;

    public PlantEntity(Long id, String name, boolean active, String description, String other, String color, String immaturePicture, String maturePicture, String startDate, String endDate, Long plantTypeId) {
        this.id = id;
        this.name = name;
        this.active = active;
        this.description = description;
        this.other = other;
        this.color = color;
        this.immaturePicture = immaturePicture;
        this.maturePicture = maturePicture;
        this.startDate = startDate;
        this.endDate = endDate;
        this.plantTypeId = plantTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImmaturePicture() {
        return immaturePicture;
    }

    public void setImmaturePicture(String immaturePicture) {
        this.immaturePicture = immaturePicture;
    }

    public String getMaturePicture() {
        return maturePicture;
    }

    public void setMaturePicture(String maturePicture) {
        this.maturePicture = maturePicture;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getPlantTypeId() {
        return plantTypeId;
    }

    public void setPlantTypeId(Long plantTypeId) {
        this.plantTypeId = plantTypeId;
    }
}
