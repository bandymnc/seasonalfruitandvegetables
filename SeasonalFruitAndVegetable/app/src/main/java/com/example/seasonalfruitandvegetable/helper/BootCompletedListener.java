package com.example.seasonalfruitandvegetable.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.seasonalfruitandvegetable.helper.alarm.AlarmHelper;
import com.example.seasonalfruitandvegetable.helper.async_tasks.BootCompletedAsyncTask;

public class BootCompletedListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())){
            new BootCompletedAsyncTask().execute(context);
        }
    }
}
