package com.example.seasonalfruitandvegetable.database.firebase;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.firebase.models.Messeage;
import com.example.seasonalfruitandvegetable.database.firebase.models.Plant;
import com.example.seasonalfruitandvegetable.database.firebase.models.Price;
import com.example.seasonalfruitandvegetable.database.firebase.models.Vitamin;
import com.example.seasonalfruitandvegetable.helper.async_tasks.RefreshPlantTableAsyncTask;
import com.example.seasonalfruitandvegetable.helper.async_tasks.RefreshPriceTableAsyncTask;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseService {
    private List<Plant> plantList = new ArrayList<>();
    private List<Price> priceList = new ArrayList<>();
    private Context context;
    private Activity activity;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference plantDateRef = database.getReference("plantData").child("lastUpdate");
    private DatabaseReference priceDateRef = database.getReference("priceData").child("lastUpdate");
    private DatabaseReference messageRef = database.getReference("messages");
    private ProgressBar progressBar;
    private TextView textView;

    public FirebaseService(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        progressBar = activity.findViewById(R.id.updateProgressBar);
        textView = activity.findViewById(R.id.updateTextView);
    }

    public void refreshPlantsData(String plantDate, String priceDate) {

        plantDateRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String lastUpdate = dataSnapshot.child("date").getValue(String.class);

                if (lastUpdate.compareTo(plantDate) > 0) {
                    setUpdateProgressBar();
                    plantTableListener(lastUpdate, priceDate);
                } else {
                    refreshPricesData(priceDate);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void plantTableListener(String plantDate, String priceDate) {
        DatabaseReference plantRef = database.getReference("plantData");
        plantRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                plantList.clear();

                for (DataSnapshot ds : dataSnapshot.child("plants").getChildren()) {
                    plantList.add(getPlantData(ds));
                }
                new RefreshPlantTableAsyncTask(plantList, plantDate, activity).execute(context);
                refreshPricesData(priceDate);
                plantRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void refreshPricesData(String date) {
        priceDateRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String lastUpdate = dataSnapshot.child("date").getValue(String.class);

                if (lastUpdate.compareTo(date) > 0) {
                    setUpdateProgressBar();
                    priceTableListener(lastUpdate);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void priceTableListener(String lastUpdate) {
        DatabaseReference priceRef = database.getReference("priceData");

        priceRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                priceList.clear();

                for (DataSnapshot d : dataSnapshot.child("prices").getChildren()) {
                    Price price = d.getValue(Price.class);
                    priceList.add(price);
                }
                new RefreshPriceTableAsyncTask(priceList, lastUpdate, activity).execute(context);
                priceRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    private Plant getPlantData(DataSnapshot ds) {
        Plant plant = new Plant();

        plant.setId(ds.child("id").getValue(Long.class));
        plant.setName(ds.child("name").getValue(String.class));
        plant.setOther(ds.child("other").getValue(String.class));
        plant.setDescription(ds.child("description").getValue(String.class));
        plant.setEndDate(ds.child("endDate").getValue(String.class));
        plant.setColor(ds.child("color").getValue(String.class));
        plant.setStartDate(ds.child("startDate").getValue(String.class));
        plant.setDeleted(ds.child("deleted").getValue(boolean.class));
        plant.setMaturePicture(ds.child("maturePicture").getValue(String.class));
        plant.setImmaturePicture(ds.child("immaturePicture").getValue(String.class));
        plant.setPlantTypeId(ds.child("plantTypeId").getValue(Long.class));

        List<Vitamin> vitaminList = new ArrayList<>();
        for (DataSnapshot d2 : ds.child("vitamins").getChildren()) {
            vitaminList.add(d2.getValue(Vitamin.class));
        }

        plant.setVitaminList(vitaminList);

        return plant;
    }

    public void writeData(Messeage messeage) {
        messageRef.push().setValue(messeage);
    }

    private void setUpdateProgressBar() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(true);
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }
}
