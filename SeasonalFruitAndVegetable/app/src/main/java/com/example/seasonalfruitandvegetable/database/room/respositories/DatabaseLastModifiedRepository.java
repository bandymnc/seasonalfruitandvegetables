package com.example.seasonalfruitandvegetable.database.room.respositories;

import androidx.lifecycle.LiveData;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.DatabaseLastModifiedDao;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;

import java.util.List;

public class DatabaseLastModifiedRepository {
    private AppExecutors appExecutors;
    private DatabaseLastModifiedDao databaseLastModifiedDao;

    public DatabaseLastModifiedRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        databaseLastModifiedDao = appDatabase.databaseLastModifiedDao();
    }
    public void insert(DatabaseLastModifiedEntity databaseLastModifiedEntity) {
        appExecutors.databaseIO().execute(() -> {
            databaseLastModifiedDao.insert(databaseLastModifiedEntity);
        });
    }

    public void update(DatabaseLastModifiedEntity databaseLastModifiedEntity) {
        appExecutors.databaseIO().execute(() -> {
            databaseLastModifiedDao.update(databaseLastModifiedEntity);
        });
    }

    public void delete(DatabaseLastModifiedEntity databaseLastModifiedEntity) {
        appExecutors.databaseIO().execute(() -> {
            databaseLastModifiedDao.delete(databaseLastModifiedEntity);
        });
    }

    public LiveData<DatabaseLastModifiedEntity> getLastModifiedDatabase(String databaseType){
        return databaseLastModifiedDao.getLastModifiedDatabase(databaseType);
    }

    public LiveData<List<DatabaseLastModifiedEntity>> getLastModifiedDatabases(){
        return databaseLastModifiedDao.getLastModifiedDatabases();
    }
}
