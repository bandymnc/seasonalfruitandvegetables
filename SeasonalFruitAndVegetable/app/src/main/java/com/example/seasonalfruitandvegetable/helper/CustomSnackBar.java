package com.example.seasonalfruitandvegetable.helper;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.example.seasonalfruitandvegetable.R;
import com.google.android.material.snackbar.Snackbar;

public class CustomSnackBar {
    private TypedValue snackBarBackgroundColor = new TypedValue();
    private TypedValue snackBarTextColor = new TypedValue();
    private View view;

    public CustomSnackBar(Context context, View view) {
        context.getTheme().resolveAttribute(R.attr.snackBarBackgroundColor, snackBarBackgroundColor, true);
        context.getTheme().resolveAttribute(R.attr.snackBarTextColor, snackBarTextColor, true);
        this.view = view;
    }

    public void showSnackBar(String message, boolean isError) {
        Snackbar snackBar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackBar.getView();
        snackBar.setDuration(8000);
        TextView snackBarTextView = snackBar.getView().findViewById(com.google.android.material.R.id.snackbar_text);

        snackBarView.setBackgroundColor(snackBarBackgroundColor.data);
        snackBar.setActionTextColor(snackBarTextColor.data);
        snackBarTextView.setTextColor(snackBarTextColor.data);

        if (isError) {
            snackBarTextView.setTextColor(Color.parseColor("#b71c1c"));
            snackBar.setActionTextColor(Color.parseColor("#b71c1c"));
            snackBarView.setBackgroundColor(Color.parseColor("#000000"));
        }

        snackBar.setAction("OK", (View v) -> {
            snackBar.dismiss();
        });
        snackBar.show();
    }
}