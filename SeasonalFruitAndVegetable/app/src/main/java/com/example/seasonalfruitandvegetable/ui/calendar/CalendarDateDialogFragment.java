package com.example.seasonalfruitandvegetable.ui.calendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.VitaminRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class CalendarDateDialogFragment extends DialogFragment {

    private CalendarViewModel calendarViewModel;
    private RecyclerView recyclerView;
    private Button button;
    private TextView titleTextView;

    public CalendarDateDialogFragment() {
    }

    public static CalendarDateDialogFragment newInstance(String title) {
        CalendarDateDialogFragment frag = new CalendarDateDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        calendarViewModel = ViewModelProviders.of(this).get(CalendarViewModel.class);
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);
        calendarViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors),new VitaminRepository(appDatabase,appExecutors));
        return inflater.inflate(R.layout.calendar_date_fragment_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String date = getArguments().getString("date", "2019-01-01");
        recyclerView = getView().findViewById(R.id.calendarDateFragmentDialogRecyclerView);
        button = getView().findViewById(R.id.calendarDateFragmentDialogButton);
        titleTextView = getView().findViewById(R.id.calendarDateFragmentTitle);
        List<PlantEntity> plantEntityList = new ArrayList<>();

        titleTextView.setText(CommonUtils.getHungarianDateFormat(date) + "\n Kapható zöldségek és gyümölcsök");

        calendarViewModel.getAllActivePlantsByDate(date).observe(this,plants->{

            for(int i=0;i<plants.size();i++){
                plantEntityList.add(plants.get(i));
            }

            CalendarDetailsAdapter calendarDetailsAdapter = new CalendarDetailsAdapter(plantEntityList);
            recyclerView.setAdapter(calendarDetailsAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        });

        button.setOnClickListener((View v)->{
            dismiss();
        });
    }
}
