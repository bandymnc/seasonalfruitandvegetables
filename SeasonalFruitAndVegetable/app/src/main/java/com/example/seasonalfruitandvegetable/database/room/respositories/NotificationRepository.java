package com.example.seasonalfruitandvegetable.database.room.respositories;

import androidx.lifecycle.LiveData;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.NotificationDao;
import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;

public class NotificationRepository {
    private AppExecutors appExecutors;
    private NotificationDao notificationDao;

    public NotificationRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        notificationDao = appDatabase.notificationDao();
    }
    public void insert(NotificationEntity notificationEntity) {
        appExecutors.databaseIO().execute(() -> {
            notificationDao.insert(notificationEntity);
        });
    }

    public void update(NotificationEntity notificationEntity) {
        appExecutors.databaseIO().execute(() -> {
            notificationDao.update(notificationEntity);
        });
    }

    public void delete(NotificationEntity notificationEntity) {
        appExecutors.databaseIO().execute(() -> {
            notificationDao.delete(notificationEntity);
        });
    }

    public LiveData<NotificationEntity> getNotificationByPlantId(Long id){
        return  notificationDao.getNotificationByPlantId(id);
    }
}
