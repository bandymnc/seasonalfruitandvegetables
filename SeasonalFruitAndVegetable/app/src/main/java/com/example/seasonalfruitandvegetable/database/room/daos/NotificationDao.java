package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;

@Dao
public interface NotificationDao {

    @Insert
    void insert(NotificationEntity notificationEntity);

    @Update
    void update(NotificationEntity notificationEntity);

    @Delete
    void delete(NotificationEntity notificationEntity);

    @Query("DELETE FROM notifications WHERE notifications.id = :notificationId")
    void deleteByNotificationId(Long notificationId);

    @Query("SELECT notifications.id,notifications.plant_id,notifications.notification_date FROM notifications WHERE notifications.plant_id = :id")
    LiveData<NotificationEntity> getNotificationByPlantId(Long id);

    @Query("SELECT * from notifications WHERE notifications.plant_id = :plantId")
    NotificationEntity getAllAsync(Long plantId);

    @Query("SELECT * FROM notifications WHERE notifications.plant_id = :plantId")
    NotificationEntity getNotificationByPlantIdSyncron(Long plantId);
}
