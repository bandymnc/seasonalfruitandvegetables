package com.example.seasonalfruitandvegetable.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.circularreveal.CircularRevealHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
    private static SimpleDateFormat databaseDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat databaseDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static SimpleDateFormat hungarianDateTimeFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
    private static SimpleDateFormat hungarianYearAndMonthFormat = new SimpleDateFormat("yyyy.MMM");

    public static Date getDateFromDbDateFormat(String date){ // 2019-01-01 -> Date
        try {
            return databaseDateFormat.parse(date);
        }catch (ParseException pex){
            System.out.println(pex);
        }
        return null;
    }

    public static Date getDateFromDateTimeFormat(String date){ // 2019-01-01 11:5 -> Date
        try {
            return databaseDateTimeFormat.parse(date);
        }catch (ParseException pex){
            System.out.println(pex);
        }
        return null;
    }

    public static String getDateTimeFromDbDate(Date date){ // Date -> 2019-01-01 10:10
        return databaseDateTimeFormat.format(date);
    }

    public static String getDatabaseDateFormat(Date date){ // Date -> 2019-01-01
        return databaseDateFormat.format(date);
    }

    public static String getHungarianYearAndMonthFormat(Date date){ // Date -> 2019.Január
        return hungarianYearAndMonthFormat.format(date);
    }

    public static String getHungarianDateFormat(String date){ // 2019-01-01 -> 2019.01.01
        return date.replace("-",".");
    }


    public static String getHungarianDateFormat(Date date){ // Date -> 2019.01.01
        return dateFormat.format(date);
    }

    public static String getHungarianDateTimeFormat(Date date){ // Date -> 2019.01.01 12:12
        return hungarianDateTimeFormat.format(date);
    }

    public static void hideKeyboard(View view, Activity activity){
        InputMethodManager imm = (InputMethodManager)activity.getSystemService((Context.INPUT_METHOD_SERVICE));
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            return 1; // Exception
        }
        return (int) l;
    }

    public static void setUpImage(String imageString, ImageView imageView,int width, int height){
        Glide.with(imageView.getContext())
                .load(Base64.decode(imageString, Base64.DEFAULT))
                .override(width,height)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = false;
        if (connectivityManager != null) {
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            isConnected = (activeNetwork != null) && (activeNetwork.isConnectedOrConnecting());
        }

        return isConnected;
    }
}
