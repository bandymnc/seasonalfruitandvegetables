package com.example.seasonalfruitandvegetable.ui.calendar;

import com.example.seasonalfruitandvegetable.database.room.data_objects.CalendarPlants;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;

import java.util.Calendar;
import java.util.List;

public class CalendarDataHelper {

    public CalendarPlants getPlants(List<PlantEntity> plantEntityList, String currentDay, String lastDay) {
        CalendarPlants calendarPlants = new CalendarPlants();

        int lastDayCounter = 0;
        int currentDayCounter = 0;
        PlantEntity[] lastDayPlantTmb = new PlantEntity[3];
        PlantEntity[] currentDayPlantTmb = new PlantEntity[3];
        int others = 0;

        for (PlantEntity p : plantEntityList) {

            if ((lastDay.compareTo(p.getStartDate()) > -1) && (lastDay.compareTo(p.getEndDate()) < 1)) {
                if (lastDayCounter == 0) {
                    lastDayPlantTmb[0] = p;
                } else if (lastDayCounter == 1) {
                    lastDayPlantTmb[1] = p;
                } else if (lastDayCounter == 2) {
                    lastDayPlantTmb[2] = p;
                }

                lastDayCounter++;
            }

            if ((currentDay.compareTo(p.getStartDate()) > -1) && (currentDay.compareTo(p.getEndDate()) < 1)) {
                if (currentDayCounter == 0) {
                    currentDayPlantTmb[0] = p;
                } else if (currentDayCounter == 1) {
                    currentDayPlantTmb[1] = p;
                } else if (currentDayCounter == 2) {
                    currentDayPlantTmb[2] = p;
                } else {
                    others++;
                }
                currentDayCounter++;
            }
        }
        calendarPlants.setOthers(others);
        calendarPlants.setLastDayPlantTmb(lastDayPlantTmb);
        calendarPlants.setToDayPlantTmb(currentDayPlantTmb);
        return calendarPlants;
    }

    public boolean writeText(PlantEntity currentPlant, PlantEntity lastdayPlant, String today, Calendar todayCalendar) {

        if (currentPlant != null && lastdayPlant != null) {
            if (!currentPlant.equals(lastdayPlant)) {
                return true;
            }
        }
        if (currentPlant != null) {
            if (today.equals(currentPlant.getStartDate()) ||
                    todayCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {

                return true;
            }
        }
        return false;
    }

    public boolean setBackgroundColor(PlantEntity currentPlant, PlantEntity lastDayPlant, String today, Calendar todayCalendar) {

        if (currentPlant != null && lastDayPlant != null) {
            return true;
        }
        if (currentPlant != null) {
            if (today.equals(currentPlant.getStartDate()) ||
                    todayCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                return true;
            }
        }
        return false;
    }
}
