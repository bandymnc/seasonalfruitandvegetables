package com.example.seasonalfruitandvegetable.ui.prices;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PricesWithPlantAndAmountUnit;
import com.example.seasonalfruitandvegetable.database.room.respositories.DatabaseLastModifiedRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.PriceRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PricesFragment extends Fragment {

    private PricesViewModel pricesViewModel;
    private RecyclerView recyclerView;
    private AutoCompleteTextView autoCompleteTextView;
    private TextView titleTextView;
    private List<String> plantNameList = new ArrayList<>();
    private List<PricesWithPlantAndAmountUnit> pricesWithPlantAndAmountUnits = new ArrayList<>();
    private CustomSnackBar customSnackBar;

    public PricesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        pricesViewModel = ViewModelProviders.of(this).get(PricesViewModel.class);
        pricesViewModel.setDependencies(new PriceRepository(appDatabase,appExecutors),new DatabaseLastModifiedRepository(appDatabase,appExecutors));

        return inflater.inflate(R.layout.fragment_prices, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customSnackBar = new CustomSnackBar(getContext(), getView());

        getLayoutItems();
        getPrices();
        autoCompleteChangeListener();
        getLastDatabaseUpdateDate();

        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        Bundle bundle = new Bundle();
        bundle.putString("first_open", CommonUtils.getDateTimeFromDbDate(Calendar.getInstance().getTime()));
        firebaseAnalytics.logEvent("prices_page", bundle);
    }

    private void getLayoutItems() {
        recyclerView = getView().findViewById(R.id.fragmentPricesRecyclerView);
        autoCompleteTextView = getView().findViewById(R.id.fragmentPricesAutocompleteTextView);
        titleTextView = getView().findViewById(R.id.fragmentPricesTitleTextView);
    }

    private void getLastDatabaseUpdateDate() {
        pricesViewModel.getLastModifiedDatabase("prices").observe(this, databaseLastModified -> {
            if (databaseLastModified != null) {
                titleTextView.setText(getString(R.string.last_update) + " " + CommonUtils.getHungarianDateFormat(databaseLastModified.getDate()));
            }
        });
    }

    private void getPrices() {

        pricesViewModel.getAllPricesWithPlantAndAmountUnit().observe(this, prices -> {

            if (prices.size() == 0) {
                customSnackBar.showSnackBar(getString(R.string.no_price_items) + "\n" + getString(R.string.try_refresh_datas), false);
            }

            pricesWithPlantAndAmountUnits.clear();
            plantNameList.clear();
            pricesWithPlantAndAmountUnits = prices;

            addListToAutocompleteTextView(pricesWithPlantAndAmountUnits);
            addListItemToRecyclerView(pricesWithPlantAndAmountUnits);
        });
    }

    private void addListItemToRecyclerView(List<PricesWithPlantAndAmountUnit> pricesWithPlantAndAmountUnitList) {

        PricesAdapter pricesAdapter = new PricesAdapter(pricesWithPlantAndAmountUnitList);
        recyclerView.setAdapter(pricesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void addListToAutocompleteTextView(List<PricesWithPlantAndAmountUnit> pricesWithPlantAndAmountUnitList) {

        for (PricesWithPlantAndAmountUnit p : pricesWithPlantAndAmountUnitList) {
            plantNameList.add(p.getPlantName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, plantNameList);
        autoCompleteTextView.setAdapter(adapter);
    }

    private void autoCompleteChangeListener() {

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    getPrices();
                    autoCompleteTextView.clearFocus();
                    CommonUtils.hideKeyboard(getView(), getActivity());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        autoCompleteTextView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {

            String selected = (String) parent.getItemAtPosition(position);
            int pos = plantNameList.indexOf(selected);

            pricesViewModel.getAllPricesWithPlantAndAmountUnitByPlantName(plantNameList.get(pos)).observe(this, price -> {
                pricesWithPlantAndAmountUnits.clear();
                pricesWithPlantAndAmountUnits = price;
                addListItemToRecyclerView(pricesWithPlantAndAmountUnits);
                CommonUtils.hideKeyboard(getView(), getActivity());
            });
        });
    }
}
