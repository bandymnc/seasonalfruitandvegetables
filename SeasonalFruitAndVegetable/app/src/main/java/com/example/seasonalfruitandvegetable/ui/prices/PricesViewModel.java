package com.example.seasonalfruitandvegetable.ui.prices;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.data_objects.PricesWithPlantAndAmountUnit;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.DatabaseLastModifiedRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.PriceRepository;

import java.util.List;

public class PricesViewModel extends ViewModel {

    private PriceRepository priceRepository;
    private DatabaseLastModifiedRepository databaseLastModifiedRepository;

    public PricesViewModel() {
    }

    public  void  setDependencies(PriceRepository priceRepository,DatabaseLastModifiedRepository databaseLastModifiedRepository){
        this.priceRepository = priceRepository;
        this.databaseLastModifiedRepository = databaseLastModifiedRepository;
    }

    public LiveData<List<PricesWithPlantAndAmountUnit>> getAllPricesWithPlantAndAmountUnit(){
        return priceRepository.getAllPricesWithPlantAndAmountUnit();
    }

    public LiveData<List<PricesWithPlantAndAmountUnit>> getAllPricesWithPlantAndAmountUnitByPlantName(String plantName){
        return priceRepository.getAllPricesWithPlantAndAmountUnitByPlantName(plantName);
    }

    public LiveData<DatabaseLastModifiedEntity> getLastModifiedDatabase(String databaseType){
        return databaseLastModifiedRepository.getLastModifiedDatabase(databaseType);
    }
}
