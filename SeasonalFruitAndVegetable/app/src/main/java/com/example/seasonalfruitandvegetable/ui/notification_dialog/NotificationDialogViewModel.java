package com.example.seasonalfruitandvegetable.ui.notification_dialog;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.NotificationRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;

public class NotificationDialogViewModel extends ViewModel {
    private PlantRepository plantRepository;
    private NotificationRepository notificationRepository;

    public NotificationDialogViewModel() {
    }

    public void setDependencies(PlantRepository plantRepository, NotificationRepository notificationRepository){
        this.plantRepository = plantRepository;
        this.notificationRepository = notificationRepository;
    }

    public LiveData<PlantEntity> getPlantById(Long id){
        return plantRepository.getPlantById(id);
    }

    public LiveData<NotificationEntity> getNotificationByPlantId(Long id){
        return notificationRepository.getNotificationByPlantId(id);
    }

    public void delete(NotificationEntity notificationEntity){
        this.notificationRepository.delete(notificationEntity);
    }

    public void insert(NotificationEntity notificationEntity){
        this.notificationRepository.insert(notificationEntity);
    }

    public void update(NotificationEntity notificationEntity){
        this.notificationRepository.update(notificationEntity);
    }
}
