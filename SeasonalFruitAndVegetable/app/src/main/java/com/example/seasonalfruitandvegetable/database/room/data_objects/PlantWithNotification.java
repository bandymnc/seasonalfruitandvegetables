package com.example.seasonalfruitandvegetable.database.room.data_objects;

public class PlantWithNotification {

    private Long plantId;
    private String name;
    private boolean active;
    private Long notificationId;
    private String notificationDate;
    private String description;
    private String other;
    private String immaturePicture;
    private String maturePicture;
    private String startDate;
    private String endDate;
    private Long plantTypeId;
    private String color;

    public PlantWithNotification(Long plantId, String name, boolean active, Long notificationId, String notificationDate, String description, String other, String immaturePicture, String maturePicture, String startDate, String endDate, Long plantTypeId, String color) {
        this.plantId = plantId;
        this.name = name;
        this.active = active;
        this.notificationId = notificationId;
        this.notificationDate = notificationDate;
        this.description = description;
        this.other = other;
        this.immaturePicture = immaturePicture;
        this.maturePicture = maturePicture;
        this.startDate = startDate;
        this.endDate = endDate;
        this.plantTypeId = plantTypeId;
        this.color = color;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getImmaturePicture() {
        return immaturePicture;
    }

    public void setImmaturePicture(String immaturePicture) {
        this.immaturePicture = immaturePicture;
    }

    public String getMaturePicture() {
        return maturePicture;
    }

    public void setMaturePicture(String maturePicture) {
        this.maturePicture = maturePicture;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getPlantTypeId() {
        return plantTypeId;
    }

    public void setPlantTypeId(Long plantTypeId) {
        this.plantTypeId = plantTypeId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
