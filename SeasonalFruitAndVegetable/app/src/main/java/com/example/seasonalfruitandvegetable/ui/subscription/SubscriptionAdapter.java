package com.example.seasonalfruitandvegetable.ui.subscription;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PlantWithNotification;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.example.seasonalfruitandvegetable.ui.notification_dialog.NotificationDialogFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ViewHolder> {
    private List<PlantWithNotification> plantWithNotificationList;
    Context context;
    FragmentActivity fragmentActivity;
    SubscriptionViewModel subscriptionViewModel;

    public SubscriptionAdapter(List<PlantWithNotification> plantWithNotificationList, Context context, FragmentActivity fragmentActivity) {
        this.plantWithNotificationList = plantWithNotificationList;
        this.context = context;
        this.fragmentActivity = fragmentActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subcription_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlantWithNotification plantWithNotification = plantWithNotificationList.get(position);

        TextView textView = holder.fragmentSubscriptionNameTextView;
        ImageButton imageButton = holder.fragmentSubscriptionImageButton;
        CheckBox checkBox = holder.fragmentSubscriptionCheckbox;
        ImageView imageView = holder.fragmentSubscriptionImageView;
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(context,appExecutors);

        checkBox.setChecked(plantWithNotification.isActive());
        textView.setText(plantWithNotification.getName());
        subscriptionViewModel = ViewModelProviders.of(fragmentActivity).get(SubscriptionViewModel.class);
        subscriptionViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors));

        if (plantWithNotification.getMaturePicture() != null) {
            CommonUtils.setUpImage(plantWithNotification.getMaturePicture(), imageView, 80, 80);
        }

        checkBox.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
            PlantEntity plantEntity = new PlantEntity(
                    plantWithNotification.getPlantId(),
                    plantWithNotification.getName(),
                    isChecked,
                    plantWithNotification.getDescription(),
                    plantWithNotification.getOther(),
                    plantWithNotification.getColor(),
                    plantWithNotification.getImmaturePicture(),
                    plantWithNotification.getMaturePicture(),
                    plantWithNotification.getStartDate(),
                    plantWithNotification.getEndDate(),
                    plantWithNotification.getPlantTypeId());
            plantEntity.setId(plantWithNotification.getPlantId());

            subscriptionViewModel.update(plantEntity);
        });

        imageButton.setOnClickListener((View v) -> {
            Bundle bundle = new Bundle();
            bundle.putLong("plantId", plantWithNotification.getPlantId());
            bundle.putBoolean("isAdapter", true);

            FragmentManager fm = fragmentActivity.getSupportFragmentManager();
            NotificationDialogFragment notificationDialogFragment = new NotificationDialogFragment();
            notificationDialogFragment.setArguments(bundle);
            notificationDialogFragment.show(fm, "fragment_edit_name");
        });
    }

    @Override
    public int getItemCount() {
        return plantWithNotificationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView fragmentSubscriptionNameTextView;
        private ImageButton fragmentSubscriptionImageButton;
        private CheckBox fragmentSubscriptionCheckbox;
        private ImageView fragmentSubscriptionImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            fragmentSubscriptionNameTextView = itemView.findViewById(R.id.subscriptionListItemNameTextView);
            fragmentSubscriptionImageButton = itemView.findViewById(R.id.subscriptionListItemImageButton);
            fragmentSubscriptionCheckbox = itemView.findViewById(R.id.subscriptionListItemCheckbox);
            fragmentSubscriptionImageView = itemView.findViewById(R.id.subscriptionListItemImageView);
        }
    }
}
