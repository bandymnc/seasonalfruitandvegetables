package com.example.seasonalfruitandvegetable.helper.async_tasks;

import java.util.concurrent.Semaphore;

public class ThreadSafe {
    private Semaphore semaphore = new Semaphore(1);
    private static ThreadSafe threadSafe;

    public ThreadSafe() {
    }

    public static ThreadSafe getInstance(){
        if (threadSafe == null){
            threadSafe = new ThreadSafe();
        }
        return threadSafe;
    }

    public void  acquire(){
        try {
            semaphore.acquire();
        }catch (InterruptedException iex){

        }
    }

    public void release(){
        semaphore.release();
    }
}
