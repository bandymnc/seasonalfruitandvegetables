package com.example.seasonalfruitandvegetable.database.room.respositories;

import androidx.lifecycle.LiveData;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.VitaminDao;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminEntity;

import java.util.List;

public class VitaminRepository {
    private AppExecutors appExecutors;
    private VitaminDao vitaminDao;

    public VitaminRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        vitaminDao = appDatabase.vitaminDao();
    }
    public void insert(VitaminEntity vitaminEntity) {
        appExecutors.databaseIO().execute(() -> {
            vitaminDao.insert(vitaminEntity);
        });
    }

    public void update(VitaminEntity vitaminEntity) {
        appExecutors.databaseIO().execute(() -> {
            vitaminDao.update(vitaminEntity);
        });
    }

    public void delete(VitaminEntity vitaminEntity) {
        appExecutors.databaseIO().execute(() -> {
            vitaminDao.delete(vitaminEntity);
        });
    }

    public LiveData<List<VitaminEntity>> getVitaminsByPlantId(Long id){
        return vitaminDao.getVitaminsByPlantId(id);
    }

    public LiveData<List<VitaminEntity>> getAllVitamin(){
        return this.vitaminDao.getAllVitamin();
    }
}
