package com.example.seasonalfruitandvegetable.database.room.respositories;

import androidx.lifecycle.LiveData;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantDao;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PlantWithNotification;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;

import java.util.List;

public class PlantRepository {
    private AppExecutors appExecutors;
    private PlantDao plantDao;

    public PlantRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        plantDao = appDatabase.plantDao();
    }
    public void insert(PlantEntity plantEntity) {
        appExecutors.databaseIO().execute(() -> {
            plantDao.insert(plantEntity);
        });
    }

    public void update(PlantEntity plantEntity) {
        appExecutors.databaseIO().execute(() -> {
            plantDao.update(plantEntity);
        });
    }

    public void delete(PlantEntity plantEntity) {
        appExecutors.databaseIO().execute(() -> {
            plantDao.delete(plantEntity);
        });
    }

    public LiveData<List<PlantEntity>> getAllActivePlantsByDate(String todayDate){
        return plantDao.getAllActivePlantsByDate(todayDate);
    }

    public LiveData<List<PlantEntity>> getPlantsByType(String plantType){
        return plantDao.getPlantsByType(plantType);
    }

    public LiveData<List<PlantEntity>> getAllActivePlant(){
        return plantDao.getAllActivePlant();
    }

    public LiveData<List<PlantEntity>> getAllAvailablePlants(String todayDate){
        return plantDao.getAllAvailablePlants(todayDate);
    }

    public LiveData<PlantEntity> getPlantById(Long id){
        return plantDao.getPlantById(id);
    }

    public LiveData<List<PlantEntity>> getPlantsByVitamins(String[] vitamins, String plantType, int size){
        return plantDao.getPlantsByVitamins(vitamins,plantType,size);
    }

    public LiveData<List<PlantWithNotification>> getPlantsWithNotification(){
        return plantDao.getPlantsWithNotification();
    }

    public LiveData<List<PlantEntity>> getAllPlant(){
        return plantDao.getAllPlant();
    }
}
