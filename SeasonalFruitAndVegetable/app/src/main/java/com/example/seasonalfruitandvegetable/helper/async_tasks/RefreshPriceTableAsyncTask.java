package com.example.seasonalfruitandvegetable.helper.async_tasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.firebase.models.Price;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.AmountUnitDao;
import com.example.seasonalfruitandvegetable.database.room.daos.DatabaseLastModifiedDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PriceDao;
import com.example.seasonalfruitandvegetable.database.room.entities.AmountUnitEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PriceEntity;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class RefreshPriceTableAsyncTask extends AsyncTask<Context, Void, Void> {
    private List<Price> priceList;
    private List<AmountUnitEntity> amountUnitEntityList;
    private AmountUnitDao amountUnitDao;
    private PriceDao priceDao;
    private AppDatabase appDatabase;
    private DatabaseLastModifiedDao databaseLastModifiedDao;
    private ThreadSafe threadSafe = ThreadSafe.getInstance();
    private String firebaseLastUpdate;
    private ProgressBar progressBar;
    private Activity activity;
    private TextView textView;

    public RefreshPriceTableAsyncTask(List<Price> priceList, String firebaseLastUpdate, Activity activity) {
        this.priceList = priceList;
        this.firebaseLastUpdate = firebaseLastUpdate;
        this.activity = activity;
    }

    @Override
    protected Void doInBackground(Context... contexts) {
        threadSafe.acquire();

        progressBar = activity.findViewById(R.id.updateProgressBar);
        textView = activity.findViewById(R.id.updateTextView);
        appDatabase = AppDatabase.getInstance(contexts[0], new AppExecutors());
        priceDao = appDatabase.priceDao();
        amountUnitDao = appDatabase.amountUnitDao();
        databaseLastModifiedDao = appDatabase.databaseLastModifiedDao();

        DatabaseLastModifiedEntity priceDatabaseEntity = databaseLastModifiedDao.getLastModifiedDatabaseSyncron("prices");

        List<PriceEntity> priceEntityList = new ArrayList<>();

        for (Price price : priceList) {
            PriceEntity priceEntity = new PriceEntity(price.getDescription(),
                    price.getMinPrice(), price.getMaxPrice(),
                    price.getPlantId(), getAmountTypeId(price.getAmountType()));

            priceEntityList.add(priceEntity);
        }

        priceDao.deleteAll();
        priceDao.insert(priceEntityList);

        priceDatabaseEntity.setDate(firebaseLastUpdate);
        databaseLastModifiedDao.update(priceDatabaseEntity);

        threadSafe.release();
        disableProgressBar();
        return null;
    }

    private Long getAmountTypeId(String amountType) {
        amountUnitEntityList = amountUnitDao.getAllAmountUnitSyncron();

        for (AmountUnitEntity amountUnitEntity : amountUnitEntityList) {
            if (amountUnitEntity.getType().equals(amountType)) {
                return amountUnitEntity.getId();
            }
        }
        AmountUnitEntity amountUnitEntity = new AmountUnitEntity(amountType);
        amountUnitDao.insert(amountUnitEntity);
        return amountUnitDao.getAmountUnitIdByTypeSyncron(amountType);
    }

    private void disableProgressBar() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setIndeterminate(false);
                progressBar.setVisibility(View.GONE);
                textView.setVisibility(View.GONE);
            }
        });
    }
}
