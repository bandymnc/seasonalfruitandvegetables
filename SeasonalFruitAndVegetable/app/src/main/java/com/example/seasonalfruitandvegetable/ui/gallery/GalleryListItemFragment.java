package com.example.seasonalfruitandvegetable.ui.gallery;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.firebase.models.Plant;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.VitaminRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.example.seasonalfruitandvegetable.ui.image.ImageFragment;
import com.example.seasonalfruitandvegetable.ui.main.MenuNavigator;
import com.example.seasonalfruitandvegetable.ui.notification_dialog.NotificationDialogFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Calendar;

public class GalleryListItemFragment extends Fragment {

    private CheckBox calendarCheckbox;
    private GalleryViewModel galleryViewModel;
    private PlantEntity plantEntity;
    private TextView dateTextView;
    private TextView descriptionTextView;
    private TextView vitaminContentTextView;
    private TextView titleTextView;
    private TextView otherTextView;
    private Button notificationButton;
    private ImageView maturePicture;
    private ImageView immaturePicture;
    private CustomSnackBar customSnackBar;

    public GalleryListItemFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        galleryViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors), new VitaminRepository(appDatabase,appExecutors));
        return inflater.inflate(R.layout.fragment_gallery_list_item, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommonUtils.hideKeyboard(getView(), getActivity());
        customSnackBar = new CustomSnackBar(getContext(), getView());

        Long plantId = getArguments().getLong("plantId", 0L);
        getPlantById(plantId);
        getLayoutItems();
    }

    private void getPlantById(Long id) {
        this.galleryViewModel.getPlantById(id).observe(this, plant -> {

            if (plant == null) {
                getActivity().onBackPressed();
            } else {
                plantEntity = plant;
                addTextToTextViews();
                buttonClickListener();
                imageViewClickListener();
                calendarCheckboxClickListener();
            }
        });
    }

    private void buttonClickListener() {
        notificationButton.setOnClickListener((View v) -> {
            String dateString = CommonUtils.getDateTimeFromDbDate(Calendar.getInstance().getTime());

            if (dateString.compareTo(plantEntity.getStartDate()) > 0) {
                customSnackBar.showSnackBar(getString(R.string.cant_subscribe_this_event), true);
            } else {
                Bundle bundle = new Bundle();
                bundle.putLong("plantId", plantEntity.getId());

                FragmentManager fm = getActivity().getSupportFragmentManager();
                NotificationDialogFragment notificationDialogFragment = new NotificationDialogFragment();
                notificationDialogFragment.setArguments(bundle);
                notificationDialogFragment.show(fm, "fragment_edit_name");
            }
        });
    }

    private void getLayoutItems() {
        dateTextView = getView().findViewById(R.id.fragmentGalleryListItemDateTextView);
        descriptionTextView = getView().findViewById(R.id.fragmentGalleryListItemDescriptionTextView);
        vitaminContentTextView = getView().findViewById(R.id.fragmentGalleryListItemVitaminContentTextView);
        notificationButton = getView().findViewById(R.id.fragmentGalleryListItemButton);
        calendarCheckbox = getView().findViewById(R.id.fragmentGalleryListItemCheckbox);
        titleTextView = getView().findViewById(R.id.fragmentGalleryListItemTitleTextView);
        otherTextView = getView().findViewById(R.id.fragmentGalleryListItemOthersTextView);
        immaturePicture = getView().findViewById(R.id.galleryListItemFragmentImmaturePicture);
        maturePicture = getView().findViewById(R.id.galleryListItemFragmentMaturePicture);
    }

    private void calendarCheckboxClickListener() {
        calendarCheckbox.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {

            setCheckboxText(isChecked);
            plantEntity.setActive(isChecked);
            galleryViewModel.updatePlant(plantEntity);

            if (isChecked) {
                FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
                Bundle bundle = new Bundle();
                bundle.putString("placed_to_calendar", plantEntity.getName());
                firebaseAnalytics.logEvent("favourite_plant", bundle);
            }
        });
    }

    private void imageViewClickListener() {
        if (plantEntity.getMaturePicture() != null) {
            maturePicture.setOnClickListener((View v) -> {
                openFragment(true);
            });
        }

        if (plantEntity.getImmaturePicture() != null) {
            immaturePicture.setOnClickListener((View v) -> {
                openFragment(false);
            });
        }
    }

    private void openFragment(boolean b) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isMature", b);
        bundle.putLong("plantId", plantEntity.getId());

        MenuNavigator menuNavigator = new MenuNavigator(getContext());
        ImageFragment imageFragment = new ImageFragment();
        imageFragment.setArguments(bundle);
        menuNavigator.replaceFragment(imageFragment, String.valueOf(R.layout.fragment_image_view), false);
    }

    private void setCheckboxText(boolean b) {
        if (b) {
            calendarCheckbox.setText(getString(R.string.event_add_calendar));
        } else {
            calendarCheckbox.setText(getString(R.string.event_take_away_calendar));
        }
    }

    private void addTextToTextViews() {
        dateTextView.setText(CommonUtils.getHungarianDateFormat(plantEntity.getStartDate())
                + " - " + CommonUtils.getHungarianDateFormat(plantEntity.getEndDate()));
        descriptionTextView.setText(plantEntity.getDescription());
        calendarCheckbox.setChecked(plantEntity.isActive());
        setCheckboxText(!calendarCheckbox.isChecked());
        titleTextView.setText(plantEntity.getName());

        if (plantEntity.getMaturePicture() != null) {
            CommonUtils.setUpImage(plantEntity.getMaturePicture(), maturePicture, 150, 150);
        }

        if (plantEntity.getImmaturePicture() != null) {
            CommonUtils.setUpImage(plantEntity.getImmaturePicture(), immaturePicture, 150, 150);
        }

        if (!plantEntity.getOther().isEmpty()) {
            otherTextView.setText(plantEntity.getOther());
        }

        this.galleryViewModel.getVitaminsByPlantId(plantEntity.getId()).observe(this, vitamins -> {
            String vitaminContent = "";

            if (vitamins != null) {
                for (int i = 0; i < vitamins.size(); i++) {

                    vitaminContent += vitamins.get(i).getType();

                    if (i != vitamins.size() - 1) {
                        vitaminContent += " , ";
                    }
                }
                vitaminContentTextView.setText(vitaminContent);
            }
        });
    }
}
