package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.data_objects.PlantWithNotification;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PlantWithPlantType;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;

import java.util.List;

@Dao
public interface PlantDao {

    @Insert
    void insert(PlantEntity plantEntity);

    @Insert
    void insert(List<PlantEntity> plantEntityList);

    @Update
    void update(PlantEntity plantEntity);

    @Delete
    void delete(PlantEntity plantEntity);

    @Query("SELECT * FROM plants WHERE plants.start_date <= :todayDate AND plants.end_date >= :todayDate ORDER BY plants.name")
    LiveData<List<PlantEntity>> getAllAvailablePlants(String todayDate);

    @Query("SELECT plants.id,plants.active,plants.plant_type_id,plants.mature_picture," +
            "plants.immature_picture, plants.color ,plants.end_date,plants.description,plants.name,plants.other,plants.start_date" +
            " FROM plants INNER JOIN plant_types ON plants.plant_type_id = plant_types.id WHERE plant_types.type_name = :plantType ORDER BY plants.name")
    LiveData<List<PlantEntity>> getPlantsByType(String plantType);

    @Query("SELECT * FROM plants WHERE plants.start_date <= :todayDate " +
            "AND plants.end_date >= :todayDate " +
            "AND plants.active = 1 ORDER BY plants.start_date")
    LiveData<List<PlantEntity>> getAllActivePlantsByDate(String todayDate);

    @Query("SELECT * FROM plants WHERE plants.active = 1 ORDER BY plants.start_date")
    LiveData<List<PlantEntity>> getAllActivePlant();

    @Query("SELECT * FROM plants WHERE plants.id = :id")
    LiveData<PlantEntity> getPlantById(Long id);

    @Query("SELECT * FROM plants")
    LiveData<List<PlantEntity>> getAllPlant();

    @Query("SELECT plants.id,plants.active, plants.color ,plants.plant_type_id,plants.mature_picture, " +
            " plants.immature_picture,plants.end_date,plants.description,plants.name,plants.other,plants.start_date " +
            " FROM plants " +
            "INNER JOIN vitamin_contents ON vitamin_contents.plant_id = plants.id " +
            "INNER JOIN vitamins ON vitamins.id = vitamin_contents.vitamin_id " +
            "INNER JOIN plant_types ON plants.plant_type_id = plant_types.id " +
            "WHERE vitamins.type IN (:vitamins) AND plant_types.type_name = :plantType GROUP BY plants.name HAVING COUNT(*) = :size ")
    LiveData<List<PlantEntity>> getPlantsByVitamins(String[] vitamins, String plantType, int size);


    @Query("SELECT plants.id as plantId, plants.color ,plants.name,plants.active, plants.end_date as endDate," +
            " plants.start_date as startDate, plants.description, plants.other, " +
            "plants.immature_picture as immaturePicture, plants.mature_picture as " +
            "maturePicture, plants.plant_type_id as plantTypeId , notifications.id as notificationId, notifications.notification_date as notificationDate " +
            " FROM plants LEFT JOIN notifications ON plants.id = notifications.plant_id" +
            " WHERE notifications.plant_id IS NOT NULL OR plants.active = 1 ORDER BY plants.name")
    LiveData<List<PlantWithNotification>> getPlantsWithNotification();

    @Query("SELECT plants.id as plantId, plants.color ,plants.name,plants.active, plants.end_date as endDate," +
            " plants.start_date as startDate, plants.description, plants.other, " +
            "plants.immature_picture as immaturePicture, plants.mature_picture as " +
            "maturePicture, plants.plant_type_id as plantTypeId , notifications.id as notificationId, notifications.notification_date as notificationDate " +
            " FROM plants INNER JOIN notifications ON plants.id = notifications.plant_id")
    List<PlantWithNotification> getAllPlantsWithNotificationSyncron();

    @Query("SELECT * FROM plants WHERE plants.name = :plantName")
    PlantEntity getPlantByNameSyncron(String plantName);

    @Query("SELECT * FROM plants WHERE plants.id = :plantId")
    PlantEntity getPlantByIdSyncron(Long plantId);
}