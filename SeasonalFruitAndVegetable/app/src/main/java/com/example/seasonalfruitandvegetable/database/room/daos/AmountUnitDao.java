package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.entities.AmountUnitEntity;

import java.util.List;

@Dao
public interface AmountUnitDao {

    @Insert
    void insert(AmountUnitEntity amountUnitEntity);

    @Insert
    void insert(List<AmountUnitEntity> amountUnitEntityList);

    @Update
    void update(AmountUnitEntity amountUnitEntity);

    @Delete
    void delete(AmountUnitEntity amountUnitEntity);

    @Query("SELECT * FROM amount_units")
    List<AmountUnitEntity> getAllAmountUnitSyncron();

    @Query("SELECT amount_units.id FROM amount_units WHERE amount_units.type = :amountType")
    Long getAmountUnitIdByTypeSyncron(String amountType);

}
