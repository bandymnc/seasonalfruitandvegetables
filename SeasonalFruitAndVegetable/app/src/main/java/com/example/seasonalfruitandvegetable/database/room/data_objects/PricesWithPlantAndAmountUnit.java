package com.example.seasonalfruitandvegetable.database.room.data_objects;

public class PricesWithPlantAndAmountUnit {

    private Long priceId;
    private Long plantId;
    private String plantName;
    private String amountUnit;
    private String description;
    private int minPrice;
    private int maxPrice;
    private String maturePicture;
    private String immaturePicture;

    public PricesWithPlantAndAmountUnit(Long priceId, Long plantId, String plantName, String amountUnit, String description, int minPrice, int maxPrice, String maturePicture, String immaturePicture) {
        this.priceId = priceId;
        this.plantId = plantId;
        this.plantName = plantName;
        this.amountUnit = amountUnit;
        this.description = description;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.maturePicture = maturePicture;
        this.immaturePicture = immaturePicture;
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getAmountUnit() {
        return amountUnit;
    }

    public void setAmountUnit(String amountUnit) {
        this.amountUnit = amountUnit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getMaturePicture() {
        return maturePicture;
    }

    public void setMaturePicture(String maturePicture) {
        this.maturePicture = maturePicture;
    }

    public String getImmaturePicture() {
        return immaturePicture;
    }

    public void setImmaturePicture(String immaturePicture) {
        this.immaturePicture = immaturePicture;
    }
}
