package com.example.seasonalfruitandvegetable.ui.gallery;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private List<PlantEntity> plantEntityList;

    public GalleryAdapter(List<PlantEntity> plantEntityList) {
        this.plantEntityList = plantEntityList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlantEntity plantEntity = plantEntityList.get(position);

        TextView homeNameTextView = holder.galleryListItemTextView;
        homeNameTextView.setText(plantEntity.getName());

        ImageView homeImageView = holder.galleryListItemImageView;

        if(plantEntity.getMaturePicture()!=null) {
            CommonUtils.setUpImage(plantEntity.getMaturePicture(),homeImageView,250,250);
        }
    }

    @Override
    public int getItemCount() {
        return plantEntityList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView galleryListItemTextView;
        public ImageView galleryListItemImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            galleryListItemTextView = itemView.findViewById(R.id.galleryListItemTextView);
            galleryListItemImageView = itemView.findViewById(R.id.galleryListItemImageView);
        }
    }
}
