package com.example.seasonalfruitandvegetable.database.room.respositories;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantTypeDao;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantTypeEntity;

public class PlantTypeRepository {
    private AppExecutors appExecutors;
    private PlantTypeDao plantTypeDao;

    public PlantTypeRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        plantTypeDao = appDatabase.plantTypeDao();
    }

    public void insert(PlantTypeEntity plantTypeEntity) {
        appExecutors.databaseIO().execute(() -> {
            plantTypeDao.insert(plantTypeEntity);
        });
    }

    public void update(PlantTypeEntity plantTypeEntity) {
        appExecutors.databaseIO().execute(() -> {
            plantTypeDao.update(plantTypeEntity);
        });
    }

    public void delete(PlantTypeEntity plantTypeEntity) {
        appExecutors.databaseIO().execute(() -> {
            plantTypeDao.delete(plantTypeEntity);
        });
    }
}
