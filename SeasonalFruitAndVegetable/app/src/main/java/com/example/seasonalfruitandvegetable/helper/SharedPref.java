package com.example.seasonalfruitandvegetable.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    SharedPreferences darkModeSharedPreferences;
    SharedPreferences analyticsSharedPreferences;

    public SharedPref(Context context) {
        darkModeSharedPreferences = context.getSharedPreferences("night_mode", Context.MODE_PRIVATE);
        analyticsSharedPreferences = context.getSharedPreferences("firebase_analytics", Context.MODE_PRIVATE);
    }

    //Save night mode state
    public void setNightMode(int state) {
        SharedPreferences.Editor editor = darkModeSharedPreferences.edit();
        editor.putInt("NightMode", state);
        editor.apply();
    }

    public int getNightModeState() {
        return darkModeSharedPreferences.getInt("NightMode", 0);
    }

    public boolean getAnalyticsSharedPreferences() {
        return analyticsSharedPreferences.getBoolean("Analytics", true);
    }

    public void setAnalyticsSharedPreferences(boolean b) {
        SharedPreferences.Editor editor = analyticsSharedPreferences.edit();
        editor.putBoolean("Analytics",b);
        editor.apply();
    }
}
