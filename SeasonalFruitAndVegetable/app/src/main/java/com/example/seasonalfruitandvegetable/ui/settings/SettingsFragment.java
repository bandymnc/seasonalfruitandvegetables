package com.example.seasonalfruitandvegetable.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.example.seasonalfruitandvegetable.helper.SharedPref;
import com.example.seasonalfruitandvegetable.ui.main.MainActivity;
import com.google.firebase.analytics.FirebaseAnalytics;

public class SettingsFragment extends Fragment {

    private RadioGroup darkModeRadioGroup;
    private SharedPref sharedPref;
    private Switch analyticsSwitch;
    private FirebaseAnalytics firebaseAnalytics;
    private CustomSnackBar customSnackBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customSnackBar = new CustomSnackBar(getContext(), getView());
        firebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        CommonUtils.hideKeyboard(getView(), getActivity());
        getController();
        settingsDarkModeRadioGroupListener();
        analyticsSwitchListener();
    }

    private void analyticsSwitchListener() {

        analyticsSwitch.setChecked(sharedPref.getAnalyticsSharedPreferences());

        analyticsSwitch.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
            sharedPref.setAnalyticsSharedPreferences(isChecked);

            Bundle bundle = new Bundle();
            if (isChecked)
                bundle.putString("used", "Kikapcsolva");
            else
                bundle.putString("used", "Bekapcsolva");

            firebaseAnalytics.logEvent("analytics", bundle);
            restartApp();
        });
    }

    private void settingsDarkModeRadioGroupListener() {

        ((RadioButton) darkModeRadioGroup.getChildAt(sharedPref.getNightModeState())).setChecked(true);

        this.darkModeRadioGroup.setOnCheckedChangeListener((RadioGroup group, int checkedId) -> {

            int radioButtonIndex = group.indexOfChild(getView().findViewById(group.getCheckedRadioButtonId()));

            if (radioButtonIndex != sharedPref.getNightModeState()) {

                sharedPref.setNightMode(radioButtonIndex);
                logAnalyticsData(radioButtonIndex);
                restartApp();
            }
        });
    }

    private void logAnalyticsData(int id) {
        Bundle bundle = new Bundle();

        if (id == 0) {
            bundle.putString("light_mode", "Light Mode");
        } else {
            bundle.putString("dark_mode", "Dark Mode");
        }
        firebaseAnalytics.logEvent("preferred_ui_mode", bundle);
    }

    private void getController() {
        this.darkModeRadioGroup = getView().findViewById(R.id.settingsDarkModeRadioGroup);
        this.sharedPref = new SharedPref(getContext());
        this.analyticsSwitch = getView().findViewById(R.id.fragmentSettingsAnalyticsSwitch);
    }

    private void restartApp() {
        Intent mStartActivity = new Intent(getContext(), MainActivity.class);
        startActivity(mStartActivity);
    }

}
