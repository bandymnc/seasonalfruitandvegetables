package com.example.seasonalfruitandvegetable.helper.alarm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.async_tasks.NotifyAsyncTask;
public class AlarmReceiver extends BroadcastReceiver {
    private String chanelId = "personal notification";

    @Override
    public void onReceive(Context context, Intent intent) {

        Long plantId = intent.getLongExtra("plantId", 1L);
        String plantName = intent.getStringExtra("plantName");

        createNotificationChannel(context);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, chanelId);
        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setContentTitle(plantName);
        builder.setContentText(context.getString(R.string.alarm_receiver_content));
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(CommonUtils.safeLongToInt(plantId), builder.build());

        new NotifyAsyncTask(plantId).execute(context);
    }

    private void createNotificationChannel(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = context.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(chanelId, name, importance);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
