package com.example.seasonalfruitandvegetable.database.firebase.models;

public class Vitamin {
    private String type;

    public Vitamin(String type) {
        this.type = type;
    }

    public Vitamin() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
