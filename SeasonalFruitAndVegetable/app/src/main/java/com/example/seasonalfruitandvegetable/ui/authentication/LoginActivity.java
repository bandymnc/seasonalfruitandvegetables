package com.example.seasonalfruitandvegetable.ui.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.example.seasonalfruitandvegetable.ui.main.MainActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity {

    private SignInButton signInButton;
    private GoogleSignInClient googleSignInClient;
    private int RC_SIGN_IN = 0;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private CustomSnackBar customSnackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        signInButton = findViewById(R.id.sign_in_button);
        progressBar = findViewById(R.id.loginProgressBar);
        customSnackBar = new CustomSnackBar(getApplicationContext(), getWindow().getDecorView());

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(getApplicationContext(), googleSignInOptions);

        signInButton.setOnClickListener((View v) -> {
            if (CommonUtils.isOnline(getApplicationContext())) {
                if (v.getId() == R.id.sign_in_button) {
                    signIn();
                }
            } else {
                customSnackBar.showSnackBar(getString(R.string.turn_on_internet), true);
            }
        });
        mAuth = FirebaseAuth.getInstance();
    }

    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);

            } catch (ApiException e) {
                Log.w("Error", "Google sign in failed", e);
                customSnackBar.showSnackBar(getString(R.string.sorry_error), true);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        progressBar.setVisibility(View.VISIBLE);

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, (Task<AuthResult> task) -> {
                    if (task.isSuccessful()) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("Error", "signInWithCredential:failure", task.getException());
                        customSnackBar.showSnackBar(getString(R.string.sorry_error), true);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        LoginActivity.this.finishAffinity();
    }
}
