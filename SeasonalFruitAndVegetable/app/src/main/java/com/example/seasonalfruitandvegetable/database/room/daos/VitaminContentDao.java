package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.entities.VitaminContentEntity;

import java.util.List;

@Dao
public interface VitaminContentDao {

    @Insert
    void insert(VitaminContentEntity vitaminContentEntity);

    @Insert
    void insert(List<VitaminContentEntity> vitaminContentEntityList);

    @Update
    void update(VitaminContentEntity vitaminContentEntity);

    @Delete
    void delete(VitaminContentEntity vitaminContentEntity);

    @Query("DELETE FROM vitamin_contents WHERE vitamin_contents.plant_id =:plantId")
    void deleteByPlantId(Long plantId);

    @Query("SELECT * FROM vitamin_contents WHERE vitamin_contents.plant_id = :plantId")
    List<VitaminContentEntity> getAllVitaminContentByPlantIdSyncron(Long plantId);
}
