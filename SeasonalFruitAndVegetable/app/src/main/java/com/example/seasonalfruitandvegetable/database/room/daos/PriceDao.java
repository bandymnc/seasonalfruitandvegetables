package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.data_objects.PricesWithPlantAndAmountUnit;
import com.example.seasonalfruitandvegetable.database.room.entities.PriceEntity;

import java.util.List;

@Dao
public interface PriceDao {

    @Insert
    void insert(PriceEntity priceEntity);

    @Insert
    void insert(List<PriceEntity> priceEntityList);

    @Update
    void update(PriceEntity priceEntity);

    @Delete
    void delete(PriceEntity priceEntity);

    @Query("DELETE FROM prices")
    void deleteAll();

    @Query("SELECT plants.id AS plantId, plants.mature_picture as maturePicture," +
            " plants.name AS plantName, amount_units.type as amountUnit, prices.description," +
            " prices.min_price AS minPrice, prices.max_price AS maxPrice FROM prices" +
            " INNER JOIN plants ON plants.id = prices.plant_id " +
            " INNER JOIN amount_units ON prices.amount_unit_id = amount_units.id")
    LiveData<List<PricesWithPlantAndAmountUnit>> getAllPricesWithPlantAndAmountUnit();

    @Query("SELECT plants.id AS plantId, plants.mature_picture as maturePicture," +
            " plants.name AS plantName, amount_units.type as amountUnit, prices.description," +
            " prices.min_price AS minPrice, prices.max_price AS maxPrice FROM prices" +
            " INNER JOIN plants ON plants.id = prices.plant_id " +
            " INNER JOIN amount_units ON prices.amount_unit_id = amount_units.id" +
            " WHERE plants.name = :plantName")
    LiveData<List<PricesWithPlantAndAmountUnit>> getAllPricesWithPlantAndAmountUnitByPlantName(String plantName);

    @Query("SELECT * FROM prices WHERE prices.plant_id = :plantId")
    PriceEntity getPriceByPlantIdSyncron(Long plantId);
}


