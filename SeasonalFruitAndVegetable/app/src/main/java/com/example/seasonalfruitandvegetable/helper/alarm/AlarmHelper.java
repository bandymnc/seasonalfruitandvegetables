package com.example.seasonalfruitandvegetable.helper.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;

import java.util.Calendar;

public class AlarmHelper {
    private Context context;

    public AlarmHelper(Context context) {
        this.context = context;
    }

    public void cancelAlarm(Long requestCode){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, safeLongToInt(requestCode)+1000, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.cancel(pendingIntent);
    }

    public void modifyAlarm(Long requestCode,PlantEntity plantEntity,Calendar calendar){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("plantId", requestCode);
        intent.putExtra("plantName",plantEntity.getName());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, safeLongToInt(requestCode)+1000, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pendingIntent);
        setBootReceiverEnabled(PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
    }

    public void setAlarm(Long requestCode, PlantEntity plantEntity,Calendar calendar){
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("plantId", requestCode);
        intent.putExtra("plantName",plantEntity.getName());

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,safeLongToInt(requestCode)+1000,intent,0);

        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pendingIntent);
        setBootReceiverEnabled(PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
    }

    private void setBootReceiverEnabled(int componentEnableState){
        ComponentName componentName = new ComponentName(context, AlarmReceiver.class);
        PackageManager packageManager = context.getPackageManager();
        packageManager.setComponentEnabledSetting(componentName,componentEnableState,PackageManager.DONT_KILL_APP);
    }

    private static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            return 1; // Exception
        }
        return (int) l;
    }
}
