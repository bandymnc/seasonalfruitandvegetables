package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantTypeEntity;

import java.util.List;

@Dao
public interface PlantTypeDao {

    @Insert
    void insert(PlantTypeEntity plantTypeEntity);

    @Insert
    void insert(List<PlantTypeEntity> plantTypeEntityList);

    @Update
    void update(PlantTypeEntity plantTypeEntity);

    @Delete
    void delete(PlantTypeEntity plantTypeEntity);
}
