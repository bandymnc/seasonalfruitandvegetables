package com.example.seasonalfruitandvegetable.database.room.entities;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.NO_ACTION;

@Entity(tableName = "notifications",
        foreignKeys = {
                @ForeignKey(
                        entity = PlantEntity.class,
                        parentColumns = "id",
                        childColumns = "plant_id",
                        onDelete = NO_ACTION)})
public class NotificationEntity {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "notification_date")
    private String notificationDate;

    @ColumnInfo(name = "plant_id")
    private Long plantId;

    public NotificationEntity(String notificationDate, Long plantId) {
        this.notificationDate = notificationDate;
        this.plantId = plantId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }
}
