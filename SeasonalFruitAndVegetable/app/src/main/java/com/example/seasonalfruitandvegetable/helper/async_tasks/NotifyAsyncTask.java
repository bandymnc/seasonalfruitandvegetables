package com.example.seasonalfruitandvegetable.helper.async_tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.NotificationDao;
import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;

public class NotifyAsyncTask extends AsyncTask<Context, Void, Void> {
    private Long plantId;

    public NotifyAsyncTask(Long plantId) {
        this.plantId = plantId;
    }

    @Override
    protected Void doInBackground(Context... contexts) {
        AppDatabase appDatabase = AppDatabase.getInstance(contexts[0],new AppExecutors());
        NotificationDao notificationDao = appDatabase.notificationDao();

        NotificationEntity notificationEntity = notificationDao.getAllAsync(plantId);
        notificationDao.delete(notificationEntity);
        return null;
    }
}
