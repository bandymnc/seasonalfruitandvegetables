package com.example.seasonalfruitandvegetable.ui.subscription;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;

public class SubscriptionFragment extends Fragment {

    private SubscriptionViewModel subscriptionViewModel;
    private RecyclerView recyclerView;
    private CustomSnackBar customSnackBar;

    public SubscriptionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        subscriptionViewModel = ViewModelProviders.of(this).get(SubscriptionViewModel.class);
        subscriptionViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors));

        return inflater.inflate(R.layout.fragment_subscription, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = getView().findViewById(R.id.fragmentSubscriptionRecyclerView);
        customSnackBar = new CustomSnackBar(getContext(),getView());

        subscriptionViewModel.getPlantsWithNotification().observe(this, plants->{
            if(plants.size()==0){
                customSnackBar.showSnackBar(getString(R.string.no_subscribed_events),false);
            }
            SubscriptionAdapter subscriptionAdapter = new SubscriptionAdapter(plants,getContext(), getActivity());
            recyclerView.setAdapter(subscriptionAdapter);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        });
    }
}
