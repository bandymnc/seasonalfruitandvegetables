package com.example.seasonalfruitandvegetable.database.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.seasonalfruitandvegetable.database.room.entities.VitaminEntity;

import java.util.List;

@Dao
public interface VitaminDao {

    @Insert
    void insert(VitaminEntity vitaminEntity);

    @Update
    void update(VitaminEntity vitaminEntity);

    @Delete
    void delete(VitaminEntity vitaminEntity);

    @Insert
    void insert(List<VitaminEntity> vitaminEntities);

    @Query("SELECT vitamins.type FROM vitamins INNER JOIN vitamin_contents ON vitamins.id = vitamin_contents.vitamin_id " +
            "WHERE vitamin_contents.plant_id = :id")
    LiveData<List<VitaminEntity>> getVitaminsByPlantId(Long id);


    @Query("SELECT vitamins.id, vitamins.type FROM vitamins ORDER BY vitamins.type")
    LiveData<List<VitaminEntity>> getAllVitamin();

    @Query("SELECT vitamins.type " +
            " FROM vitamins INNER JOIN vitamin_contents ON vitamin_contents.vitamin_id = vitamins.id " +
            " WHERE vitamin_contents.plant_id = :plantId")
    List<String> getAllVitaminByPlantIdSyncron(Long plantId);

    @Query("SELECT vitamins.id FROM vitamins WHERE vitamins.type = :vitaminType")
    Long getVitaminIdSyncron(String vitaminType);
}
