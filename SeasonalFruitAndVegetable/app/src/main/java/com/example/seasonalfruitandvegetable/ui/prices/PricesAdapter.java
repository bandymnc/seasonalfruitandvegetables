package com.example.seasonalfruitandvegetable.ui.prices;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PricesWithPlantAndAmountUnit;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.util.List;

public class PricesAdapter extends RecyclerView.Adapter<PricesAdapter.ViewHolder> {
    List<PricesWithPlantAndAmountUnit> pricesWithPlantAndAmountUnits;

    public PricesAdapter(List<PricesWithPlantAndAmountUnit> pricesWithPlantAndAmountUnits) {
        this.pricesWithPlantAndAmountUnits = pricesWithPlantAndAmountUnits;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.prices_fragment_list_item, parent, false);

        PricesAdapter.ViewHolder viewHolder = new PricesAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PricesWithPlantAndAmountUnit pricesWithPlantAndAmountUnit = pricesWithPlantAndAmountUnits.get(position);

        TextView priceTextView = holder.priceTextView;
        TextView descriptionTextView = holder.descriptionTextView;
        TextView plantNameTextView = holder.plantNameTextView;
        TextView amountUnitTextView = holder.amountUnitTextView;
        ImageView maturePictureImageView = holder.maturePictureImageView;

        priceTextView.setText("Ár: " + pricesWithPlantAndAmountUnit.getMinPrice() + "Ft - " + pricesWithPlantAndAmountUnit.getMaxPrice() + "Ft");
        descriptionTextView.setText(pricesWithPlantAndAmountUnit.getDescription());
        plantNameTextView.setText(pricesWithPlantAndAmountUnit.getPlantName());
        amountUnitTextView.setText("Mennyiségi egység: " + pricesWithPlantAndAmountUnit.getAmountUnit());

        if (pricesWithPlantAndAmountUnit.getMaturePicture() != null) {
            CommonUtils.setUpImage(pricesWithPlantAndAmountUnit.getMaturePicture(), maturePictureImageView, 110, 110);
        }
    }

    @Override
    public int getItemCount() {
        return pricesWithPlantAndAmountUnits.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView priceTextView;
        public TextView descriptionTextView;
        public TextView plantNameTextView;
        public TextView amountUnitTextView;
        public ImageView maturePictureImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            plantNameTextView = itemView.findViewById(R.id.pricesFragmentListItemNameTextView);
            priceTextView = itemView.findViewById(R.id.pricesFragmentListItemPriceTextView);
            descriptionTextView = itemView.findViewById(R.id.pricesFragmentListItemDescriptionTextView);
            amountUnitTextView = itemView.findViewById(R.id.pricesFragmentListItemAmountUnitTextView);
            maturePictureImageView = itemView.findViewById(R.id.pricesFragmentListItemMaturePictureImageView);
        }
    }
}
