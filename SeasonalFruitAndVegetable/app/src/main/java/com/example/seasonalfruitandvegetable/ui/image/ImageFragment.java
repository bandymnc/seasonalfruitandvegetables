package com.example.seasonalfruitandvegetable.ui.image;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.github.chrisbanes.photoview.PhotoView;

public class ImageFragment extends DialogFragment {
    private ImageViewModel imageViewModel;
    PhotoView photoView;

    public ImageFragment() {
    }

    public static ImageFragment newInstance(String title) {
        ImageFragment frag = new ImageFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);
        imageViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors));

        return inflater.inflate(R.layout.fragment_image_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Long plantId = getArguments().getLong("plantId", 0L);
        boolean isMature = getArguments().getBoolean("isMature");

        photoView = (PhotoView) getView().findViewById(R.id.photo_view);
        if (isMature) {
            imageViewModel.getPlantById(plantId).observe(this, plant -> {
                CommonUtils.setUpImage(plant.getMaturePicture(), photoView, 1920, 1080);
            });
        } else {
            imageViewModel.getPlantById(plantId).observe(this, plant -> {
                CommonUtils.setUpImage(plant.getImmaturePicture(), photoView, 1920, 1080);
            });
        }
    }

}
