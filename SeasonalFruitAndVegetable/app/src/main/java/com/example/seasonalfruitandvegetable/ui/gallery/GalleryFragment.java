package com.example.seasonalfruitandvegetable.ui.gallery;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.VitaminRepository;
import com.example.seasonalfruitandvegetable.helper.CustomSnackBar;
import com.example.seasonalfruitandvegetable.helper.RecyclerTouchListener;
import com.example.seasonalfruitandvegetable.ui.main.MenuNavigator;
import com.example.seasonalfruitandvegetable.ui.notification_dialog.NotificationDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends Fragment {

    private RecyclerView galleryRecyclerView;
    private Button vegetableButton;
    private Button fruitButton;
    private GalleryViewModel galleryViewModel;
    private AutoCompleteTextView galleryAutocomplete;
    private List<PlantEntity> plantEntityListFilledType = new ArrayList<>();
    private List<PlantEntity> plantEntityList = new ArrayList<>();
    private Button fillButton;
    private String[] vitaminNameList;
    private String[] filledVitaminName;
    private boolean[] checkedItems;
    private ArrayList<Integer> userItems = new ArrayList<>();
    private boolean isVegetable = true;
    private String plantType;
    private CustomSnackBar customSnackBar;
    private List<String> stringList = new ArrayList<>();
    private TypedValue pressedButtonColor = new TypedValue();
    private TypedValue buttonColor = new TypedValue();

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppExecutors appExecutors = new AppExecutors();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(),appExecutors);

        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        galleryViewModel.setDependencies(new PlantRepository(appDatabase,appExecutors), new VitaminRepository(appDatabase,appExecutors));
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getContext().getTheme().resolveAttribute(R.attr.pressedButtonColor, pressedButtonColor, true);
        getContext().getTheme().resolveAttribute(R.attr.buttonColor, buttonColor, true);
        customSnackBar = new CustomSnackBar(getContext(),getView());

        getLayoutItems();
        setDefaultValue();
        switchChangeListener();
        recyclerViewClickListener();
        getPlantsByType(false);
        getPlantsWithoutType();
        autoCompleteChangeListener();
        addItemsToSpinner();
    }

    private void setDefaultValue(){
        plantType = getString(R.string.plant_type_fruit);
        isVegetable = false;
        fruitButton.setBackgroundColor(pressedButtonColor.data);
    }

    private void addItemsToSpinner() {

        galleryViewModel.getAllVitamin().observe(this, vitamins -> {
            vitaminNameList = new String[vitamins.size()];

            for (int i = 0; i < vitamins.size(); i++) {
                vitaminNameList[i] = vitamins.get(i).getType();
            }

            checkedItems = new boolean[vitaminNameList.length];

            fillButton.setOnClickListener((View v) -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle(getString(R.string.choose_vitamins));
                builder.setMultiChoiceItems(vitaminNameList, checkedItems, (DialogInterface dialog, int position, boolean isChecked) -> {

                    if (isChecked) {
                        if (!userItems.contains(position)) {
                            userItems.add(position);
                        }
                    } else if (userItems.contains(position)) {
                        userItems.remove(Integer.valueOf(position));
                    }
                });
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.gallery_fill), (DialogInterface dialog, int position) -> {
                    filledVitaminName = new String[userItems.size()];

                    if (userItems.size() == 0) {
                        getPlantsByType(isVegetable);
                    } else {
                        for (int i = 0; i < userItems.size(); i++) {
                            filledVitaminName[i] = vitaminNameList[userItems.get(i)];
                        }
                        getPlantsByVitamins(filledVitaminName);
                    }
                });

                builder.setNegativeButton(getString(R.string.cancel), (DialogInterface dialog, int which) -> {
                    dialog.dismiss();
                });

                builder.setNeutralButton(getString(R.string.clear_all), (DialogInterface dialog, int position) -> {
                    deleteConditions();
                    getPlantsByType(isVegetable);
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            });
        });
    }

    private void deleteConditions() {
        for (int i = 0; i < checkedItems.length; i++) {
            checkedItems[i] = false;
        }
        userItems.clear();
    }

    private void getPlantsByVitamins(String[] vitamins) {
        plantEntityListFilledType.clear();

        galleryViewModel.getPlantsByVitamins(vitamins, plantType, vitamins.length).observe(this, plants -> {

            plantEntityListFilledType = plants;
            addListItemToRecyclerView();
        });
    }

    private void getLayoutItems() {
        galleryRecyclerView = getView().findViewById(R.id.galleryRecyclerView);
        vegetableButton = getView().findViewById(R.id.fragmentGalleryVegetableButton);
        fruitButton = getView().findViewById(R.id.fragmentFalleryFruitButton);
        galleryAutocomplete = getView().findViewById(R.id.galleryAutoCompleteTextView);
        fillButton = getView().findViewById(R.id.fragmentGalleryFillButton);
    }

    private void switchChangeListener() {

        fruitButton.setOnClickListener((View v)->{
            isVegetable = false;
            getPlantsByType(false);
            deleteConditions();
            fruitButton.setBackgroundColor(pressedButtonColor.data);
            vegetableButton.setBackgroundColor(buttonColor.data);
        });

        vegetableButton.setOnClickListener((View v)->{
            isVegetable = true;
            getPlantsByType(true);
            deleteConditions();
            fruitButton.setBackgroundColor(buttonColor.data);
            vegetableButton.setBackgroundColor(pressedButtonColor.data);
        });
    }

    private void autoCompleteChangeListener() {
        galleryAutocomplete.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {

            String selected = (String) parent.getItemAtPosition(position);
            int pos = stringList.indexOf(selected);
            openNotificationFragment(pos, plantEntityList);
        });
    }

    private void addAutocompleteList(List<String> stringList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, stringList);
        galleryAutocomplete.setAdapter(adapter);
    }

    private void getPlantsByType(boolean isVegetable) {
        plantEntityListFilledType.clear();
        plantType = isVegetable ? "Zöldség" : "Gyümölcs";

        galleryViewModel.getPlantsByType(plantType).observe(this, plants -> {
            plantEntityListFilledType = plants;

            addListItemToRecyclerView();
        });
    }

    private void getPlantsWithoutType() {
        plantEntityList.clear();
        galleryViewModel.getAllPlant().observe(this, plants -> {
            createStringListToData(plants);
            addAutocompleteList(stringList);
            plantEntityList = plants;
        });
    }

    private void recyclerViewClickListener() {
        galleryRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), galleryRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                openNotificationFragment(position, plantEntityListFilledType);
            }

            @Override
            public void onLongClick(View view, int position) {
                openNotificationDialogFragment(position);
            }
        }));
    }

    private void createStringListToData(List<PlantEntity> allPlants) {
        stringList.clear();
        for (int i = 0; i < allPlants.size(); i++) {
            stringList.add(allPlants.get(i).getName());
        }
    }

    //Fragment megnyitása, és a kiválasztott növény id-jét átadni neki
    private void openNotificationFragment(int position, List<PlantEntity> plantEntityList) {
        Bundle bundle = new Bundle();
        bundle.putLong("plantId", plantEntityList.get(position).getId());

        MenuNavigator menuNavigator = new MenuNavigator(getContext());
        GalleryListItemFragment galleryListItemFragment = new GalleryListItemFragment();
        galleryListItemFragment.setArguments(bundle);
        String tag = String.valueOf(R.layout.fragment_gallery_list_item);
        menuNavigator.replaceFragment(galleryListItemFragment, tag,false);
    }

    private void openNotificationDialogFragment(int position) {
        Bundle bundle = new Bundle();
        bundle.putLong("plantId", plantEntityListFilledType.get(position).getId());

        if (getActivity() != null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            NotificationDialogFragment notificationDialogFragment = new NotificationDialogFragment();
            notificationDialogFragment.setArguments(bundle);
            notificationDialogFragment.show(fm, "notification");
        }
    }

    private void addListItemToRecyclerView() {
        if (plantEntityListFilledType.size() == 0) {
            customSnackBar.showSnackBar( getString(R.string.no_item_to_display), false);
        }

        GalleryAdapter galleryAdapter = new GalleryAdapter(plantEntityListFilledType);
        galleryRecyclerView.setAdapter(galleryAdapter);
        galleryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }
}
