package com.example.seasonalfruitandvegetable.helper.async_tasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.firebase.models.Plant;
import com.example.seasonalfruitandvegetable.database.firebase.models.Vitamin;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.DatabaseLastModifiedDao;
import com.example.seasonalfruitandvegetable.database.room.daos.NotificationDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PriceDao;
import com.example.seasonalfruitandvegetable.database.room.daos.VitaminContentDao;
import com.example.seasonalfruitandvegetable.database.room.daos.VitaminDao;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PriceEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminContentEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminEntity;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.alarm.AlarmHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RefreshPlantTableAsyncTask extends AsyncTask<Context, Void, Void> {
    private List<Plant> newPlantList;
    private AlarmHelper alarmHelper;
    private ThreadSafe threadSafe = ThreadSafe.getInstance();
    private String firebaseLastUpdate;
    private VitaminContentDao vitaminContentDao;
    private NotificationDao notificationDao;
    private PriceDao priceDao;
    private VitaminDao vitaminDao;
    private PlantDao plantDao;
    private ProgressBar progressBar;
    private TextView textView;
    private Activity activity;

    public RefreshPlantTableAsyncTask(List<Plant> newPlantList, String firebaseLastUpdate, Activity activity) {
        this.newPlantList = newPlantList;
        this.firebaseLastUpdate = firebaseLastUpdate;
        this.activity = activity;
    }

    @Override
    protected Void doInBackground(Context... contexts) {
        threadSafe.acquire();

        alarmHelper = new AlarmHelper(contexts[0]);
        AppDatabase appDatabase = AppDatabase.getInstance(contexts[0], new AppExecutors());
        plantDao = appDatabase.plantDao();
        vitaminDao = appDatabase.vitaminDao();
        vitaminContentDao = appDatabase.vitaminContentDao();
        notificationDao = appDatabase.notificationDao();
        DatabaseLastModifiedDao databaseLastModifiedDao = appDatabase.databaseLastModifiedDao();
        priceDao = appDatabase.priceDao();
        progressBar = activity.findViewById(R.id.updateProgressBar);
        textView = activity.findViewById(R.id.updateTextView);

        DatabaseLastModifiedEntity plantDatabaseEntity = databaseLastModifiedDao.getLastModifiedDatabaseSyncron("plants");

        for (Plant newPlant : newPlantList) {

            PlantEntity oldPlantEntity = plantDao.getPlantByNameSyncron(newPlant.getName());

            if (oldPlantEntity == null && !newPlant.isDeleted()) { // Nincs ilyen nevű  és nem törölt,tehát el kell menteni

                PlantEntity plantEntity = new PlantEntity(newPlant.getId(), newPlant.getName(), false,
                        newPlant.getDescription(), newPlant.getOther(), newPlant.getColor(), newPlant.getImmaturePicture(), newPlant.getMaturePicture(),
                        newPlant.getStartDate(), newPlant.getEndDate(), newPlant.getPlantTypeId());
                plantDao.insert(plantEntity);

                saveNewPlantVitamins(newPlant.getVitaminList(), newPlant.getId(), vitaminDao);

            } else { // Van már ilyen nevű
                if (!newPlant.isDeleted()) {// nincs törölve
                    if (!newPlant.getStartDate().equals(oldPlantEntity.getStartDate()) || // Valami változás van az alap adatokban TODO ellenőrizni a képeket is
                            !newPlant.getOther().equals(oldPlantEntity.getOther()) ||
                            !newPlant.getStartDate().equals(oldPlantEntity.getEndDate()) ||
                            !newPlant.getImmaturePicture().equals(oldPlantEntity.getImmaturePicture()) ||
                            !newPlant.getMaturePicture().equals(oldPlantEntity.getMaturePicture()) ||
                            !newPlant.getColor().equals(oldPlantEntity.getColor()) ||
                            !newPlant.getDescription().equals(oldPlantEntity.getDescription())) {

                        PlantEntity plantEntity = new PlantEntity(oldPlantEntity.getId(), newPlant.getName(), oldPlantEntity.isActive(),
                                newPlant.getDescription(), newPlant.getOther(), newPlant.getColor(), newPlant.getImmaturePicture(), newPlant.getMaturePicture(),
                                newPlant.getStartDate(), newPlant.getEndDate(), newPlant.getPlantTypeId());

                        plantDao.update(plantEntity);
                    }
                    if (!vitaminListIsSame(newPlant, oldPlantEntity.getId(), vitaminDao)) { // Változott a vitamin lista
                        vitaminContentDao.deleteByPlantId(oldPlantEntity.getId());
                        saveNewPlantVitamins(newPlant.getVitaminList(), oldPlantEntity.getId(), vitaminDao);
                    }

                    if (!oldPlantEntity.getStartDate().equals(newPlant.getStartDate())) {
                        checkNotifications(oldPlantEntity.getStartDate(), newPlant.getStartDate(), oldPlantEntity);
                    }
                } else { // Törölve lett
                    handleDelete(newPlant.getId());
                }
            }
        }
        plantDatabaseEntity.setDate(firebaseLastUpdate);
        databaseLastModifiedDao.update(plantDatabaseEntity);
        threadSafe.release();
        disableProgressBar();
        return null;
    }

    private void checkNotifications(String oldStartDateString, String newStartDateString, PlantEntity plantEntity) {

        NotificationEntity notificationEntity = notificationDao.getNotificationByPlantIdSyncron(plantEntity.getId());

        if (notificationEntity != null) { // Volt elmentve értesítés

            Date todayDate = Calendar.getInstance().getTime();
            Date oldNotificationDate = CommonUtils.getDateFromDateTimeFormat(notificationEntity.getNotificationDate());
            Date newStartDate = CommonUtils.getDateFromDbDateFormat(newStartDateString);
            Date oldStartDate = CommonUtils.getDateFromDbDateFormat(oldStartDateString);

            if (todayDate.compareTo(newStartDate) > 0) { // elmúlt a kezdő dátum
                notificationDao.deleteByNotificationId(notificationEntity.getId());
                alarmHelper.cancelAlarm(plantEntity.getId());
            } else {// nem múlt el

                Calendar newNotificationCalendar = Calendar.getInstance();
                newNotificationCalendar.setTime(newStartDate);

                if (newStartDate.compareTo(oldStartDate) > 0) { // az új kezdő dátum nagyobb mint a régi
                    Long diff = oldStartDate.getTime() - oldNotificationDate.getTime();
                    newNotificationCalendar.add(Calendar.DATE, -CommonUtils.safeLongToInt(TimeUnit.MILLISECONDS.toDays(diff)));
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(oldNotificationDate);

                newNotificationCalendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
                newNotificationCalendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));

                alarmHelper.modifyAlarm(plantEntity.getId(), plantEntity, newNotificationCalendar);
                notificationDao.delete(notificationEntity);
                notificationDao.insert(new NotificationEntity(CommonUtils.getDateTimeFromDbDate(newNotificationCalendar.getTime()), plantEntity.getId()));
            }
        }
    }

    private void saveNewPlantVitamins(List<Vitamin> vitaminList, Long plantId, VitaminDao vitaminDao) {

        for (Vitamin vitamin : vitaminList) {

            Long vitaminId = vitaminDao.getVitaminIdSyncron(vitamin.getType());

            if (vitaminId == null) {
                vitaminDao.insert(new VitaminEntity(vitamin.getType()));
                vitaminId = vitaminDao.getVitaminIdSyncron(vitamin.getType());
            }
            VitaminContentEntity vitaminContentEntity = new VitaminContentEntity(vitaminId, plantId);
            vitaminContentDao.insert(vitaminContentEntity);
        }
    }

    //Megnézi, hogy a régi és az új vitamin lista egyezik e, ha igen, akkor true
    private boolean vitaminListIsSame(Plant plant, Long oldPlantId, VitaminDao vitaminDao) {
        List<String> vitamins = vitaminDao.getAllVitaminByPlantIdSyncron(oldPlantId);

        if (vitamins == null) { // nincs hozzá elmentve vitamin
            return false;
        }

        List<String> newPlantVitamins = new ArrayList<>();

        for (Vitamin v : plant.getVitaminList()) {
            newPlantVitamins.add(v.getType());
        }

        List<String> copyVitamins = vitamins;
        List<String> copyNewVitamins = newPlantVitamins;

        copyVitamins.removeAll(copyNewVitamins);
        newPlantVitamins.removeAll(vitamins);

        if (copyVitamins.size() == 0 && newPlantVitamins.size() == 0) {
            return true;
        }
        return false;
    }

    private void handleDelete(Long plantId) {
        List<VitaminContentEntity> vitaminContentEntityList = vitaminContentDao.getAllVitaminContentByPlantIdSyncron(plantId);
        NotificationEntity notificationEntity = notificationDao.getNotificationByPlantIdSyncron(plantId);
        PriceEntity priceEntity = priceDao.getPriceByPlantIdSyncron(plantId);

        for (VitaminContentEntity v : vitaminContentEntityList) { // vitamin kapcsolatok törlése
            vitaminContentDao.deleteByPlantId(v.getPlantId());
        }

        if (notificationEntity != null) { // értesítés törlése, és az adatbázisból is
            notificationDao.delete(notificationEntity);
            alarmHelper.cancelAlarm(plantId);
        }

        if (priceEntity != null) {
            priceDao.delete(priceEntity);
        }
        PlantEntity plantEntity = plantDao.getPlantByIdSyncron(plantId);

        if (plantEntity != null) {
            plantDao.delete(plantEntity);
        }
    }

    private void disableProgressBar() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setIndeterminate(false);
                progressBar.setVisibility(View.GONE);
                textView.setVisibility(View.GONE);
            }
        });
    }
}
