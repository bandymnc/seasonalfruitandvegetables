package com.example.seasonalfruitandvegetable.database.room.respositories;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.VitaminContentDao;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminContentEntity;

public class VitaminContentRepository {
    private AppExecutors appExecutors;
    private VitaminContentDao vitaminContentDao;

    public VitaminContentRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        vitaminContentDao = appDatabase.vitaminContentDao();
    }

    public void insert(VitaminContentEntity vitaminContentEntity) {
        appExecutors.databaseIO().execute(() -> {
            vitaminContentDao.insert(vitaminContentEntity);
        });
    }

    public void update(VitaminContentEntity vitaminContentEntity) {
        appExecutors.databaseIO().execute(() -> {
            vitaminContentDao.update(vitaminContentEntity);
        });
    }

    public void delete(VitaminContentEntity vitaminContentEntity) {
        appExecutors.databaseIO().execute(() -> {
            vitaminContentDao.delete(vitaminContentEntity);
        });
    }
}
