package com.example.seasonalfruitandvegetable.database.room.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.NO_ACTION;
import static androidx.room.ForeignKey.RESTRICT;

@Entity(tableName = "prices",
        foreignKeys = {
                @ForeignKey(
                        entity = PlantEntity.class,
                        parentColumns = "id",
                        childColumns = "plant_id",
                        onDelete = NO_ACTION),
                @ForeignKey(
                        entity = AmountUnitEntity.class,
                        parentColumns = "id",
                        childColumns = "amount_unit_id",
                        onDelete = NO_ACTION)})
public class PriceEntity {
    @PrimaryKey(autoGenerate = false)
    private Long id;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "min_price")
    private int minPrice;

    @ColumnInfo(name = "max_price")
    private int maxPrice;

    @ColumnInfo(name = "plant_id")
    private Long plantId;

    @ColumnInfo(name = "amount_unit_id")
    private Long amountUnitId;

    public PriceEntity(String description, int minPrice, int maxPrice, Long plantId, Long amountUnitId) {
        this.description = description;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.plantId = plantId;
        this.amountUnitId = amountUnitId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    public Long getAmountUnitId() {
        return amountUnitId;
    }

    public void setAmountUnitId(Long amountUnitId) {
        this.amountUnitId = amountUnitId;
    }
}
