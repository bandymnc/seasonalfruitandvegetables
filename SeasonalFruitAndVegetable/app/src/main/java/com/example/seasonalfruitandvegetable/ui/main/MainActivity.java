package com.example.seasonalfruitandvegetable.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.firebase.FirebaseService;
import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.DatabaseLastModifiedRepository;
import com.example.seasonalfruitandvegetable.helper.SharedPref;
import com.example.seasonalfruitandvegetable.ui.authentication.LoginActivity;
import com.example.seasonalfruitandvegetable.ui.home.HomeFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView bottomNavigationView;
    private NavigationView drawerNavigationView;
    private DrawerLayout drawerLayout;
    private MenuNavigator menuNavigator;
    private MainActivityViewModel mainActivityViewModel;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = new SharedPref(getApplicationContext());

        handleNightMode();
        handleAnalytics();

        if (GoogleSignIn.getLastSignedInAccount(this) != null) {
            AppExecutors appExecutors = new AppExecutors();
            AppDatabase appDatabase = AppDatabase.getInstance(getApplicationContext(),appExecutors);

            menuNavigator = new MenuNavigator(this);
            mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
            mainActivityViewModel.setDependencies(new DatabaseLastModifiedRepository(appDatabase,appExecutors));
            setContentView(R.layout.activity_main);
            defineDrawerLayout();
            defineBottomNavigationMenu();
            setUpUserData();
            checkUpdates();

        } else {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        }
    }

    private void checkUpdates() {
        FirebaseService firebaseService = new FirebaseService(getApplicationContext(), this);
        LiveData<List<DatabaseLastModifiedEntity>> listLiveData = mainActivityViewModel.getLastModifiedDatabases();

        listLiveData.observe(this, new Observer<List<DatabaseLastModifiedEntity>>() {
            @Override
            public void onChanged(List<DatabaseLastModifiedEntity> databaseLastModifiedEntities) {
                String plantLastModified = "";
                String priceLastModified = "";

                for (DatabaseLastModifiedEntity de : databaseLastModifiedEntities) {
                    if (de.getDatabaseType().equals("plants"))
                        plantLastModified = de.getDate();
                    else
                        priceLastModified = de.getDate();
                }
                firebaseService.refreshPlantsData(plantLastModified, priceLastModified);
                listLiveData.removeObserver(this);
            }
        });
    }

    private void defineDrawerLayout() {
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerNavigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        drawerNavigationView.setNavigationItemSelectedListener(this);
    }

    private void defineBottomNavigationMenu() {
        bottomNavigationView = findViewById(R.id.nav_view2);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        String tag = String.valueOf(R.layout.fragment_home);
        menuNavigator.replaceFragment(new HomeFragment(), tag, true);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }

        Fragment fragment = mainActivityViewModel.onBackPressed(this, menuNavigator);
        if (fragment != null) {
            setClickButton(fragment);
        } else {
            MainActivity.this.finishAffinity();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        setBottomNavMenuUnCheckable();
        int menuId = mainActivityViewModel.doDrawerNavigation(item, this, menuNavigator, this);
        drawerNavigationView.getMenu().getItem(menuId).setChecked(true);
        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            setBottomNavMenuCheckable();
            setDrawableMenuUnCheckable();

            return mainActivityViewModel.doBottomNavigation(menuItem, menuNavigator);
        }
    };

    private void setUpUserData() {
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);
        View view = drawerNavigationView.getHeaderView(0);

        TextView emailTextView = view.findViewById(R.id.userEmailTextView);
        TextView nameTextView = view.findViewById(R.id.userNameTextView);
        ImageView imageView = view.findViewById(R.id.userPictureImageView);

        emailTextView.setText(googleSignInAccount.getEmail());
        nameTextView.setText(googleSignInAccount.getDisplayName());
        imageView.setImageURI(googleSignInAccount.getPhotoUrl());
    }

    private void setBottomNavMenuUnCheckable() {
        bottomNavigationView.getMenu().setGroupCheckable(0, false, true);
    }

    private void setBottomNavMenuCheckable() {
        bottomNavigationView.getMenu().setGroupCheckable(0, true, true);
    }

    private void setDrawableMenuUnCheckable() {
        drawerNavigationView.getMenu().getItem(0).setChecked(false);
        drawerNavigationView.getMenu().getItem(1).setChecked(false);
        drawerNavigationView.getMenu().getItem(2).setChecked(false);
    }

    private void setClickButton(Fragment fragment) {
        setDrawableMenuUnCheckable();
        setBottomNavMenuUnCheckable();
        if (fragment instanceof HomeFragment) {
            setBottomNavMenuCheckable();
            bottomNavigationView.getMenu().getItem(0).setChecked(true);
        } else {
            setBottomNavMenuCheckable();
            bottomNavigationView.getMenu().getItem(1).setChecked(true);
        }
    }

    private void handleNightMode() {
        if (sharedPref.getNightModeState() == 0) {
            setTheme(R.style.AppTheme);
        } else {
            setTheme(R.style.darkTheme);
        }
    }

    private void handleAnalytics() {
        FirebaseAnalytics.getInstance(getApplicationContext()).setAnalyticsCollectionEnabled(sharedPref.getAnalyticsSharedPreferences());
    }
}
