package com.example.seasonalfruitandvegetable.database.room.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "databases_last_modified")
public class DatabaseLastModifiedEntity {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "database_type")
    private String databaseType;

    public DatabaseLastModifiedEntity(String date, String databaseType) {
        this.date = date;
        this.databaseType = databaseType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(String databaseType) {
        this.databaseType = databaseType;
    }
}
