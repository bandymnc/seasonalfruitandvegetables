package com.example.seasonalfruitandvegetable.helper.async_tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.NotificationDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantDao;
import com.example.seasonalfruitandvegetable.database.room.data_objects.PlantWithNotification;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;
import com.example.seasonalfruitandvegetable.helper.alarm.AlarmHelper;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BootCompletedAsyncTask extends AsyncTask<Context, Void, Void> {
    private ThreadSafe threadSafe = ThreadSafe.getInstance();

    @Override
    protected Void doInBackground(Context... contexts) {

        threadSafe.acquire();

        AppDatabase appDatabase = AppDatabase.getInstance(contexts[0], new AppExecutors());
        PlantDao plantDao = appDatabase.plantDao();
        NotificationDao notificationDao = appDatabase.notificationDao();
        List<PlantWithNotification> plantWithNotificationList = plantDao.getAllPlantsWithNotificationSyncron();
        AlarmHelper alarmHelper = new AlarmHelper(contexts[0]);

        for (PlantWithNotification p : plantWithNotificationList) {
            Calendar calendar = Calendar.getInstance();
            PlantEntity plantEntity = new PlantEntity(p.getPlantId(), p.getName(), false, p.getDescription(), p.getOther(), p.getColor(), p.getImmaturePicture(),
                    p.getMaturePicture(), p.getStartDate(), p.getEndDate(), p.getPlantTypeId());

            Date today = Calendar.getInstance().getTime();
            Date notificationDate = CommonUtils.getDateFromDateTimeFormat(p.getNotificationDate());

            if (today.compareTo(notificationDate) > 0) { // az értesítés elmúlt
                notificationDao.deleteByNotificationId(p.getNotificationId());
            } else {
                calendar.setTime(notificationDate);
                alarmHelper.setAlarm(p.getPlantId(), plantEntity, calendar);
            }
        }
        threadSafe.release();
        return null;
    }
}
