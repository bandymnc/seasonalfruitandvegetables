package com.example.seasonalfruitandvegetable.ui.main;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.seasonalfruitandvegetable.R;

public class MenuNavigator {

    private Context context;

    public MenuNavigator(Context context) {
        this.context = context;
    }

    public void replaceFragment(Fragment fragment, String tag, boolean addBackStack) {
        if (fragment != null) {
            doFragmentTransaction(fragment, tag, addBackStack);
        }
    }

    private void doFragmentTransaction(Fragment fragment, String tag, boolean deleteBackStack) {
        FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.transation_right, R.anim.transation_left, R.anim.transation_right, R.anim.transation_left);

        if (deleteBackStack)
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        fragmentTransaction.add(R.id.fragment_container, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }
}
