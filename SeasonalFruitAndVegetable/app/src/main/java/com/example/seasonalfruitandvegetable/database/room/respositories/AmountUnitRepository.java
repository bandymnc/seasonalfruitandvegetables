package com.example.seasonalfruitandvegetable.database.room.respositories;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.AmountUnitDao;
import com.example.seasonalfruitandvegetable.database.room.entities.AmountUnitEntity;

public class AmountUnitRepository {
    private AppExecutors appExecutors;
    private AmountUnitDao amountUnitDao;

    public AmountUnitRepository(AppDatabase appDatabase, AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
        amountUnitDao = appDatabase.amountUnitDao();
    }

    public void insert(AmountUnitEntity amountUnitEntity) {
        appExecutors.databaseIO().execute(() -> {
            amountUnitDao.insert(amountUnitEntity);
        });
    }

    public void update(AmountUnitEntity amountUnitEntity) {
        appExecutors.databaseIO().execute(() -> {
            amountUnitDao.update(amountUnitEntity);
        });
    }
}
