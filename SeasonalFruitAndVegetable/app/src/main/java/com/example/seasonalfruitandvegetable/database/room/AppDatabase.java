package com.example.seasonalfruitandvegetable.database.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.seasonalfruitandvegetable.database.room.daos.AmountUnitDao;
import com.example.seasonalfruitandvegetable.database.room.daos.DatabaseLastModifiedDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantDao;
import com.example.seasonalfruitandvegetable.database.room.daos.NotificationDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantTypeDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PriceDao;
import com.example.seasonalfruitandvegetable.database.room.daos.VitaminContentDao;
import com.example.seasonalfruitandvegetable.database.room.daos.VitaminDao;
import com.example.seasonalfruitandvegetable.database.room.entities.AmountUnitEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.NotificationEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantTypeEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PriceEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminContentEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.VitaminEntity;
import com.example.seasonalfruitandvegetable.database.room.mock_data.MockDataGenerator;

@Database(entities = {
        PlantEntity.class,
        DatabaseLastModifiedEntity.class,
        VitaminContentEntity.class,
        NotificationEntity.class,
        VitaminEntity.class,
        PlantTypeEntity.class,
        AmountUnitEntity.class,
        PriceEntity.class
}, version = 4)

public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(final Context context, final AppExecutors appExecutors) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "app_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);

                            MockDataGenerator mockDataGenerator = new MockDataGenerator();
                            mockDataGenerator.fillDatabaseWithMockData(instance, appExecutors, context);
                        }

                        @Override
                        public void onOpen(@NonNull SupportSQLiteDatabase db) {
                            super.onOpen(db);

                            //MockDataGenerator mockDataGenerator = new MockDataGenerator();
                            //mockDataGenerator.fillDatabaseWithMockData(instance, appExecutors, context);
                        }
                    })
                    .build();
        }

        return instance;
    }

    public abstract PlantTypeDao plantTypeDao();

    public abstract PlantDao plantDao();

    public abstract NotificationDao notificationDao();

    public abstract DatabaseLastModifiedDao databaseLastModifiedDao();

    public abstract VitaminContentDao vitaminContentDao();

    public abstract VitaminDao vitaminDao();

    public abstract PriceDao priceDao();

    public abstract AmountUnitDao amountUnitDao();
}
