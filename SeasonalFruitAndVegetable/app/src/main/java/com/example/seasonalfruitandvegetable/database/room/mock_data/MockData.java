package com.example.seasonalfruitandvegetable.database.room.mock_data;

import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantTypeEntity;

import java.util.List;

public class MockData {
    private List<DatabaseLastModifiedEntity> databaseLastModifiedEntityList;
    private List<PlantTypeEntity> plantTypeEntityList;

    public List<DatabaseLastModifiedEntity> getDatabaseLastModifiedEntityList() {
        return databaseLastModifiedEntityList;
    }

    public List<PlantTypeEntity> getPlantTypeEntityList() {
        return plantTypeEntityList;
    }

    public MockData(List<DatabaseLastModifiedEntity> databaseLastModifiedEntityList, List<PlantTypeEntity> plantTypeEntityList) {
        this.databaseLastModifiedEntityList = databaseLastModifiedEntityList;
        this.plantTypeEntityList = plantTypeEntityList;
    }
}
