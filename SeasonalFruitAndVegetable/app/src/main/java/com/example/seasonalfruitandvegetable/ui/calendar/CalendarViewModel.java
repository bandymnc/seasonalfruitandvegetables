package com.example.seasonalfruitandvegetable.ui.calendar;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;
import com.example.seasonalfruitandvegetable.database.room.respositories.VitaminRepository;

import java.util.List;

public class CalendarViewModel extends ViewModel {

    PlantRepository plantRepository;
    VitaminRepository vitaminRepository;

    public CalendarViewModel() {
    }

    public void setDependencies(PlantRepository plantRepository, VitaminRepository vitaminRepository){
        this.plantRepository = plantRepository;
        this.vitaminRepository = vitaminRepository;
    }

    public LiveData<List<PlantEntity>> getAllActivePlantsByDate(String date){
        return plantRepository.getAllActivePlantsByDate(date);
    }

    public LiveData<List<PlantEntity>> getAllActivePlant(){
        return plantRepository.getAllActivePlant();
    }
}
