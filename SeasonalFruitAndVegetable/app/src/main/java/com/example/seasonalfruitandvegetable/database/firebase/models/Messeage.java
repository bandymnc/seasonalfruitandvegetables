package com.example.seasonalfruitandvegetable.database.firebase.models;

public class Messeage {
    private String title;
    private String message;
    private String userEmail;
    private String dateTime;

    public Messeage(String title, String message, String userEmail, String dateTime) {
        this.title = title;
        this.message = message;
        this.userEmail = userEmail;
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
