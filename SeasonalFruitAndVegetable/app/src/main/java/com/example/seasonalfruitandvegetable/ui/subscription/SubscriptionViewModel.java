package com.example.seasonalfruitandvegetable.ui.subscription;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.database.room.data_objects.PlantWithNotification;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.PlantRepository;

import java.util.List;

public class SubscriptionViewModel extends ViewModel {

    private PlantRepository plantRepository;

    public SubscriptionViewModel() {
    }

    public void setDependencies(PlantRepository plantRepository){
        this.plantRepository = plantRepository;
    }

    public LiveData<List<PlantWithNotification>> getPlantsWithNotification(){
        return plantRepository.getPlantsWithNotification();
    }

    public void update(PlantEntity plantEntity){
        plantRepository.update(plantEntity);
    }
}
