package com.example.seasonalfruitandvegetable.database.room.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.NO_ACTION;

@Entity(tableName = "vitamin_contents",
        foreignKeys = {
                @ForeignKey(entity = VitaminEntity.class,
                        parentColumns = "id",
                        childColumns = "vitamin_id",
                        onDelete = NO_ACTION),
                @ForeignKey(entity = PlantEntity.class,
                        parentColumns = "id",
                        childColumns = "plant_id",
                        onDelete = NO_ACTION)})
public class VitaminContentEntity {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "vitamin_id")
    private Long vitaminId;

    @ColumnInfo(name = "plant_id")
    private Long plantId;

    public VitaminContentEntity(Long vitaminId, Long plantId) {
        this.vitaminId = vitaminId;
        this.plantId = plantId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVitaminId() {
        return vitaminId;
    }

    public void setVitaminId(Long vitaminId) {
        this.vitaminId = vitaminId;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }
}
