package com.example.seasonalfruitandvegetable.database.room.mock_data;

import android.content.Context;

import com.example.seasonalfruitandvegetable.database.room.AppDatabase;
import com.example.seasonalfruitandvegetable.database.room.AppExecutors;
import com.example.seasonalfruitandvegetable.database.room.daos.DatabaseLastModifiedDao;
import com.example.seasonalfruitandvegetable.database.room.daos.PlantTypeDao;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantTypeEntity;

import java.util.ArrayList;
import java.util.List;

public class MockDataGenerator {
    Context context;

    public void fillDatabaseWithMockData(AppDatabase database, AppExecutors appExecutors, Context context) {
        this.context = context;

        DatabaseLastModifiedDao databaseLastModifiedDao = database.databaseLastModifiedDao();
        PlantTypeDao plantTypeDao = database.plantTypeDao();

        MockDataGenerator mockDataGenerator = new MockDataGenerator();
        MockData mockData = mockDataGenerator.getMockData(context);

        appExecutors.databaseIO().execute(() -> {
            plantTypeDao.insert(mockData.getPlantTypeEntityList());
            databaseLastModifiedDao.insert(mockData.getDatabaseLastModifiedEntityList());
        });
    }

    private MockData getMockData(Context context) {
        List<DatabaseLastModifiedEntity> databaseLastModifiedEntityList = new ArrayList<>();
        List<PlantTypeEntity> plantTypeEntityList = new ArrayList<>();

        databaseLastModifiedEntityList.add(new DatabaseLastModifiedEntity("2018-01-01", "plants"));
        databaseLastModifiedEntityList.add(new DatabaseLastModifiedEntity("2018-01-01", "prices"));

        plantTypeEntityList.add(new PlantTypeEntity("Zöldség"));
        plantTypeEntityList.add(new PlantTypeEntity("Gyümölcs"));


        return new MockData(databaseLastModifiedEntityList, plantTypeEntityList);
    }
}

