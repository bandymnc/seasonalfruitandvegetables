package com.example.seasonalfruitandvegetable.ui.calendar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.util.List;

public class CalendarDetailsAdapter extends RecyclerView.Adapter<CalendarDetailsAdapter.ViewHolder>{

List<PlantEntity> plantEntityList;

    public CalendarDetailsAdapter(List<PlantEntity> plantEntityList) {
        this.plantEntityList = plantEntityList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calendar_details_list_item,parent,false);

        CalendarDetailsAdapter.ViewHolder viewHolder = new CalendarDetailsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlantEntity plantEntity = plantEntityList.get(position);

        TextView nameTextView = holder.nameTextView;
        TextView availabilityTextView = holder.availabilityTextView;
        TextView seasonTextView = holder.seasonTextView;

        if(plantEntity.getPlantTypeId()==1L) {
            seasonTextView.setText("Zöldség");
        }else{
            seasonTextView.setText("Gyümölcs");
        }

        nameTextView.setText(plantEntity.getName());
        availabilityTextView.setText("Elérhető: " + CommonUtils.getHungarianDateFormat(plantEntity.getStartDate())
                + " - " + CommonUtils.getHungarianDateFormat(plantEntity.getEndDate()));
    }

    @Override
    public int getItemCount() {
        return plantEntityList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView availabilityTextView;
        public TextView seasonTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.calendarDetailsListItemNameTextView);
            availabilityTextView = itemView.findViewById(R.id.calendarDetailslistItemAvailabilityTextView);
            seasonTextView = itemView.findViewById(R.id.calendarDetailslistItemSeasonTextView);
        }
    }
}
