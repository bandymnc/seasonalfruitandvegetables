package com.example.seasonalfruitandvegetable.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.entities.DatabaseLastModifiedEntity;
import com.example.seasonalfruitandvegetable.database.room.respositories.DatabaseLastModifiedRepository;
import com.example.seasonalfruitandvegetable.ui.about.AboutFragment;
import com.example.seasonalfruitandvegetable.ui.authentication.LoginActivity;
import com.example.seasonalfruitandvegetable.ui.calendar.CalendarFragment;
import com.example.seasonalfruitandvegetable.ui.gallery.GalleryFragment;
import com.example.seasonalfruitandvegetable.ui.home.HomeFragment;
import com.example.seasonalfruitandvegetable.ui.prices.PricesFragment;
import com.example.seasonalfruitandvegetable.ui.settings.SettingsFragment;
import com.example.seasonalfruitandvegetable.ui.subscription.SubscriptionFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class MainActivityViewModel extends ViewModel {
    private int actualMenuId;
    private FirebaseAuth mAuth;
    private DatabaseLastModifiedRepository databaseLastModifiedRepository;

    public MainActivityViewModel() {
    }

    public void setDependencies(DatabaseLastModifiedRepository databaseLastModifiedRepository){
        this.databaseLastModifiedRepository = databaseLastModifiedRepository;
    }

    public Fragment getIndexOfPreviousFragment(MainActivity context) {

        FragmentManager fragmentManager = context.getSupportFragmentManager();
        String tag = fragmentManager.getBackStackEntryAt(
                fragmentManager.getBackStackEntryCount() - 2).getName();

        return fragmentManager.findFragmentByTag(tag);
    }

    public Fragment onBackPressed(MainActivity mainActivity, MenuNavigator menuNavigator) {

        if (mainActivity.getSupportFragmentManager().getBackStackEntryCount() > 1) {
            mainActivity.getSupportFragmentManager().popBackStack();
            mainActivity.getSupportFragmentManager().beginTransaction().commit();

            Fragment fragment = getIndexOfPreviousFragment(mainActivity);
            actualMenuId = fragment.getId();

            return fragment;
        }
        if (actualMenuId != R.id.bottom_menu_home) {
            menuNavigator.replaceFragment(new HomeFragment(), String.valueOf(R.layout.fragment_home),true);
            actualMenuId = R.id.bottom_menu_home;
            return new HomeFragment();
        }
        return null;
    }

    public boolean doBottomNavigation(MenuItem item, MenuNavigator menuNavigator) {

        if (item.getItemId() != actualMenuId) {
            switch (item.getItemId()) {
                case R.id.bottom_menu_calendar:
                    actualMenuId = R.id.bottom_menu_calendar;
                    menuNavigator.replaceFragment(new CalendarFragment(), String.valueOf(R.layout.fragment_calendar),true);
                    break;
                case R.id.bottom_menu_gallery:
                    actualMenuId = R.id.bottom_menu_gallery;
                    menuNavigator.replaceFragment(new GalleryFragment(), String.valueOf(R.layout.fragment_gallery),true);
                    break;
                case R.id.bottom_menu_home:
                    actualMenuId = R.id.bottom_menu_home;
                    menuNavigator.replaceFragment(new HomeFragment(), String.valueOf(R.layout.fragment_home),true);
                    break;
                case R.id.bottom_menu_prices:
                    actualMenuId = R.id.bottom_menu_prices;
                    menuNavigator.replaceFragment(new PricesFragment(), String.valueOf(R.layout.fragment_prices),true);
                    break;
                default:
                    actualMenuId = R.id.bottom_menu_subscription;
                    menuNavigator.replaceFragment(new SubscriptionFragment(), String.valueOf(R.layout.fragment_subscription),true);
                    break;
            }
        }
        return true;
    }

    public int doDrawerNavigation(MenuItem item, Context context, MenuNavigator menuNavigator,
                                  MainActivity mainActivity) {
        int id = 2;
        DrawerLayout drawer = mainActivity.findViewById(R.id.drawer_layout);

        if (item.getItemId() != actualMenuId) {

            switch (item.getItemId()) {
                case R.id.nav_drawer_contact:
                    actualMenuId = R.id.nav_drawer_contact;
                    menuNavigator.replaceFragment(new AboutFragment(), String.valueOf(R.layout.fragment_about),true);
                    id = 1;
                    break;
                case R.id.nav_drawer_settings:
                    actualMenuId = R.id.nav_drawer_settings;
                    menuNavigator.replaceFragment(new SettingsFragment(), String.valueOf(R.layout.fragment_settings),true);
                    id = 0;
                    break;
                default:
                    signOut(context, mainActivity);
                    break;
            }
        }
        drawer.closeDrawer(GravityCompat.START);
        return id;
    }

    private void signOut(Context context, Activity activity) {
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, gso);

        mAuth.signOut();
        googleSignInClient.signOut().addOnCompleteListener(activity, (Task<Void> task) -> {
            activity.finish();
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        });
    }

    public LiveData<List<DatabaseLastModifiedEntity>> getLastModifiedDatabases(){
        return databaseLastModifiedRepository.getLastModifiedDatabases();
    }

    public LiveData<DatabaseLastModifiedEntity> getLastModifiedDatabase(String type){
        return databaseLastModifiedRepository.getLastModifiedDatabase(type);
    }
}
