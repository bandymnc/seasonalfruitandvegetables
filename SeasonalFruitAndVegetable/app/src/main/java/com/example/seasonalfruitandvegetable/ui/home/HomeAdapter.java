package com.example.seasonalfruitandvegetable.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seasonalfruitandvegetable.R;
import com.example.seasonalfruitandvegetable.database.room.entities.PlantEntity;
import com.example.seasonalfruitandvegetable.helper.CommonUtils;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private List<PlantEntity> plantEntityList;
    private Context context;

    public HomeAdapter(List<PlantEntity> plantEntityList, Context context) {
        this.plantEntityList = plantEntityList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.available_plant_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter.ViewHolder holder, int position) {

        PlantEntity plantEntity = plantEntityList.get(position);
        TextView homeNameTextView = holder.homeNameTextView;
        TextView homeDetailsTextView = holder.homeDetailsTextView;
        TextView homeAvailabilityTextView = holder.homeAvailabilityTextView;
        ImageView homeImageView = holder.homeImageView;

        homeNameTextView.setText(plantEntity.getName());
        homeDetailsTextView.setText(
                CommonUtils.getHungarianDateFormat(plantEntity.getStartDate()) + " - " + CommonUtils.getHungarianDateFormat(plantEntity.getEndDate()));

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(CommonUtils.getDateFromDbDateFormat(plantEntity.getEndDate()));
        Long diff = endCalendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        homeAvailabilityTextView.setText(context.getString(R.string.home_availability_day) + " " + TimeUnit.MILLISECONDS.toDays(diff) + " " + context.getString(R.string.home_day));

        if (plantEntity.getMaturePicture() != null) {
            CommonUtils.setUpImage(plantEntity.getMaturePicture(), homeImageView, 110, 110);
        }
    }

    @Override
    public int getItemCount() {
        return plantEntityList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView homeNameTextView;
        public TextView homeDetailsTextView;
        public TextView homeAvailabilityTextView;
        public ImageView homeImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            homeNameTextView = itemView.findViewById(R.id.homeNameTextView);
            homeDetailsTextView = itemView.findViewById(R.id.homeDetailsTextView);
            homeAvailabilityTextView = itemView.findViewById(R.id.homeAvailabilityTextView);
            homeImageView = itemView.findViewById(R.id.homeImageView);
        }
    }
}
